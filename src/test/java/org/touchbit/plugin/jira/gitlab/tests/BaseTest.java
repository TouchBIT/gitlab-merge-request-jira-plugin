/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests;

import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.jupiter.api.BeforeEach;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;

import static org.touchbit.plugin.jira.gitlab.JLab.clearCache;
import static org.touchbit.plugin.jira.gitlab.JLab.setConfiguration;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Oleg Shaburov on 22.04.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("WeakerAccess")
public abstract class BaseTest {

    private static final String USER_NAME = "admin";
    private static final String GROUP = "group";
    private static final java.net.URI URI;

    static {
        new MockComponentWorker().init();
        try {
            URI = new URI("http://jira.test:8080");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    @BeforeEach
    public void clear() {
        setConfiguration(null);
        clearCache();
    }

    protected static UserManager getMockUserManager() {
        UserManager manager = mock(UserManager.class);
        when(manager.getRemoteUsername(mock(HttpServletRequest.class))).thenReturn(USER_NAME);
        when(manager.isSystemAdmin(USER_NAME)).thenReturn(true);
        when(manager.isUserInGroup(USER_NAME, GROUP)).thenReturn(true);
        return manager;
    }

    protected static LoginUriProvider getMockLoginUriProvider() {
        LoginUriProvider provider = mock(LoginUriProvider.class);
        when(provider.getLoginUri(URI)).thenReturn(URI);
        return provider;
    }

    protected static TemplateRenderer getMockTemplateRenderer() {
        return mock(TemplateRenderer.class);
    }

    protected static PluginSettingsFactory getMockPluginSettingsFactory() {
        PluginSettingsFactory factory = mock(PluginSettingsFactory.class);
        when(factory.createSettingsForKey("org.touchbit.plugin.jira.gitlab.config"))
                .thenReturn(mock(PluginSettings.class));
        return factory;
    }

}
