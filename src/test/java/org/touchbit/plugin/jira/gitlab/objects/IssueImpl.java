/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.objects;

import com.atlassian.jira.bc.project.component.ProjectComponent;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.ModifiedValue;
import com.atlassian.jira.issue.MutableIssue;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.renderer.IssueRenderContext;
import com.atlassian.jira.issue.issuetype.IssueType;
import com.atlassian.jira.issue.label.Label;
import com.atlassian.jira.issue.priority.Priority;
import com.atlassian.jira.issue.resolution.Resolution;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;

import javax.annotation.Nullable;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("ALL")
public class IssueImpl  implements MutableIssue {
    @Override
    public void setProject(org.ofbiz.core.entity.GenericValue genericValue) {}
    @Override
    public void setProjectObject(Project project) {}
    @Override
    public void setProjectId(Long aLong) throws IllegalArgumentException {}
    @Override
    public void setIssueType(IssueType issueType) {}
    @Override
    public void setIssueTypeObject(IssueType issueType) {}
    @Override
    public void setIssueTypeId(String s) {}
    @Override
    public void setSummary(String s) {}
    @Override
    public void setAssignee(ApplicationUser applicationUser) {}
    @Override
    public void setComponent(Collection<ProjectComponent> collection) {}
    @Override
    public void setAssigneeId(String s) {}
    @Override
    public void setReporter(ApplicationUser applicationUser) {}
    @Override
    public void setReporterId(String s) {}
    @Override
    public void setDescription(String s) {}
    @Override
    public void setEnvironment(String s) {}
    @Override
    public void setAffectedVersions(Collection<Version> collection) {}
    @Override
    public void setFixVersions(Collection<Version> collection) {}
    @Override
    public void setDueDate(Timestamp timestamp) {}
    @Override
    public void setSecurityLevelId(Long aLong) {}
    @Override
    public void setSecurityLevel(org.ofbiz.core.entity.GenericValue genericValue) {}
    @Override
    public void setPriority(@Nullable Priority priority) {}
    @Override
    public void setPriorityObject(@Nullable Priority priority) {}
    @Override
    public void setPriorityId(String s) {}
    @Override
    public void setResolution(Resolution resolution) {}
    @Override
    public void setResolutionObject(Resolution resolution) {}
    @Override
    public void setKey(String s) {}
    @Override
    public void setNumber(Long aLong) {}
    @Override
    public void setVotes(Long aLong) {}
    @Override
    public void setWatches(Long aLong) {}
    @Override
    public void setCreated(Timestamp timestamp) {}
    @Override
    public void setUpdated(Timestamp timestamp) {}
    @Override
    public void setResolutionDate(Timestamp timestamp) {}
    @Override
    public void setWorkflowId(Long aLong) {}
    @Override
    public void setCustomFieldValue(CustomField customField, Object o) {}
    @Override
    public void setStatus(Status status) {}
    @Override
    public void setStatusObject(Status status) {}
    @Override
    public void setStatusId(String s) {}
    @Override
    public void resetModifiedFields() {}
    @Override
    public void setOriginalEstimate(Long aLong) {}
    @Override
    public void setTimeSpent(Long aLong) {}
    @Override
    public void setEstimate(Long aLong) {}
    @Override
    public void setExternalFieldValue(String s, Object o) {}
    @Override
    public void setExternalFieldValue(String s, Object o, Object o1) {}
    @Override
    public void setParentId(Long aLong) {}
    @Override
    public void setParentObject(Issue issue) {}
    @Override
    public void setResolutionId(String s) {}
    @Override
    public void setLabels(Set<Label> set) {}
    @Override
    public Map<String, ModifiedValue> getModifiedFields() {return null;}
    @Override
    public void store() {}
    @Override
    public Long getId() {return null;}
    @Override
    public org.ofbiz.core.entity.GenericValue getProject() {return null;}
    @Override
    public Project getProjectObject() {return null;}
    @Override
    public Long getProjectId() {return null;}
    @Override
    public IssueType getIssueType() {return null;}
    @Override
    public IssueType getIssueTypeObject() {return null;}
    @Override
    public String getIssueTypeId() {return null;}
    @Override
    public String getSummary() {return null;}
    @Override
    public ApplicationUser getAssigneeUser() {return null;}
    @Override
    public ApplicationUser getAssignee() {return null;}
    @Override
    public String getAssigneeId() {return null;}
    @Override
    public Collection<ProjectComponent> getComponentObjects() {return null;}
    @Override
    public Collection<ProjectComponent> getComponents() {return null;}
    @Override
    public ApplicationUser getReporterUser() {return null;}
    @Override
    public ApplicationUser getReporter() {return null;}
    @Override
    public String getReporterId() {return null;}
    @Override
    public ApplicationUser getCreator() {return null;}
    @Override
    public String getCreatorId() {return null;}
    @Override
    public String getDescription() {return null;}
    @Override
    public String getEnvironment() {return null;}
    @Override
    public Collection<Version> getAffectedVersions() {return null;}
    @Override
    public Collection<Version> getFixVersions() {return null;}
    @Override
    public Timestamp getDueDate() {return null;}
    @Override
    public org.ofbiz.core.entity.GenericValue getSecurityLevel() {return null;}
    @Override
    public Long getSecurityLevelId() {return null;}
    @Nullable
    @Override
    public Priority getPriority() {return null;}
    @Nullable
    @Override
    public Priority getPriorityObject() {return null;}
    @Override
    public String getResolutionId() {return null;}
    @Override
    public Resolution getResolution() {return null;}
    @Override
    public Resolution getResolutionObject() {return null;}
    @Override
    public String getKey() {return null;}
    @Override
    public Long getNumber() {return null;}
    @Override
    public Long getVotes() {return null;}
    @Override
    public Long getWatches() {return null;}
    @Override
    public Timestamp getCreated() {return null;}
    @Override
    public Timestamp getUpdated() {return null;}
    @Override
    public Timestamp getResolutionDate() {return null;}
    @Override
    public Long getWorkflowId() {return null;}
    @Override
    public Object getCustomFieldValue(CustomField customField) {return null;}
    @Override
    public Status getStatus() {return null;}
    @Override
    public String getStatusId() {return null;}
    @Override
    public Status getStatusObject() {return null;}
    @Override
    public Long getOriginalEstimate() {return null;}
    @Override
    public Long getEstimate() {return null;}
    @Override
    public Long getTimeSpent() {return null;}
    @Override
    public Object getExternalFieldValue(String s) {return null;}
    @Override
    public boolean isSubTask() {return false;}
    @Override
    public Long getParentId() {return null;}
    @Override
    public boolean isCreated() {return false;}
    @Override
    public Issue getParentObject() {return null;}
    @Override
    public org.ofbiz.core.entity.GenericValue getParent() {return null;}
    @Override
    public Collection<org.ofbiz.core.entity.GenericValue> getSubTasks() {return null;}
    @Override
    public Collection<Issue> getSubTaskObjects() {return null;}
    @Override
    public boolean isEditable() {return false;}
    @Override
    public IssueRenderContext getIssueRenderContext() {return null;}
    @Override
    public Collection<Attachment> getAttachments() {return null;}
    @Override
    public Set<Label> getLabels() {return null;}
    @Override
    public String getString(String s) {return null;}
    @Override
    public Timestamp getTimestamp(String s) {return null;}
    @Override
    public Long getLong(String s) {return null;}
    @Override
    public org.ofbiz.core.entity.GenericValue getGenericValue() {return null;}
}