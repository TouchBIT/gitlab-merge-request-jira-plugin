/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;
import org.touchbit.plugin.jira.gitlab.model.jlab.MrWebStatus;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.touchbit.plugin.jira.gitlab.model.jlab.MrWebStatus.*;
import static org.hamcrest.core.Is.is;

/**
 * Created by Oleg Shaburov on 20.04.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("MrWebStatus tests")
class MrWebStatusTests extends BaseTest {

    @Test
    void unitTest_20180420211242() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("WIP: test");
        mr.setState(OPENED_STATE);
        mr.setMergeStatus(CAN_BE_MERGED);
        assertThat(MrWebStatus.get(mr), is(WIP.name()));
    }

    @Test
    void unitTest_20180420211954() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("WIP: test");
        mr.setState(OPENED_STATE);
        mr.setMergeStatus("test");
        assertThat(MrWebStatus.get(mr), is(WIP.name()));
    }

    @Test
    void unitTest_20180420211607() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("test");
        mr.setState(OPENED_STATE);
        mr.setMergeStatus(CAN_BE_MERGED);
        assertThat(MrWebStatus.get(mr), is(REVIEW.name()));
    }

    @Test
    void unitTest_20180420211914() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("test");
        mr.setState(OPENED_STATE);
        mr.setMergeStatus("test");
        assertThat(MrWebStatus.get(mr), is(CONFLICT.name()));
    }

    @Test
    void unitTest_20180420212343() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("test");
        mr.setState(MERGED_STATE);
        assertThat(MrWebStatus.get(mr), is(MERGED.name()));
    }

    @Test
    void unitTest_20180420212420() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("test");
        mr.setState(CLOSED_STATE);
        assertThat(MrWebStatus.get(mr), is(CLOSED.name()));
    }

    @Test
    void unitTest_20180420212439() {
        assertThat(MrWebStatus.get(null), is(ERROR.name()));
    }

    @Test
    void unitTest_20180420212559() {
        MergeRequest mr = new MergeRequest();
        mr.setTitle("test");
        mr.setState("TEST_STATE");
        assertThat(MrWebStatus.get(mr), is("TEST_STATE"));
    }

}
