/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.touchbit.plugin.jira.gitlab.utils.StringUtils.base64Decode;
import static org.touchbit.plugin.jira.gitlab.utils.StringUtils.base64Encode;
import static org.hamcrest.core.Is.is;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("StringUtils tests")
class StringUtilsTests extends BaseTest {

    @Test
    void unitTest_20180421204302() {
        assertThat(base64Decode("IA=="), is(" "));
    }

    @Test
    void unitTest_20180421204520() {
        assertThat(base64Encode(" "), is("IA=="));
    }

    @Test
    void unitTest_20180421224512() {
        assertThat(base64Encode(null), is(""));
    }

    @Test
    void unitTest_20180422003023() {
        assertThat(base64Decode(null), is(""));
    }

}
