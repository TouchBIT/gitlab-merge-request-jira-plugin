/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import org.hamcrest.Matcher;
import org.hamcrest.StringDescription;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleg Shaburov on 15.04.2018
 * shaburov.o.a@gmail.com
 */
public class SoftlyAssert {

    private static final Logger LOG  = LoggerFactory.getLogger("TestLog");

    private List<AssertionException> errors = new ArrayList<>();

    public <T> void assertThat(T actual, Matcher<? super T> matcher) {
        assertThat("", actual, matcher);
    }

    public <T> void assertThat(String reason, T actual, Matcher<? super T> matcher) {
        StringDescription description = new StringDescription();
        if (matcher.matches(actual)) {
            description.appendText(reason)
                    .appendText("\n      expected: ")
                    .appendDescriptionOf(matcher)
                    .appendText("\n        actual: ");
            matcher.describeMismatch(actual, description);
            if (LOG != null) {
                LOG.info("[Successful checking] {}\n", description);
            }
        } else {
            description.appendText(reason)
                    .appendText("\n      expected: ")
                    .appendDescriptionOf(matcher)
                    .appendText("\n        actual: ");
            // Большие месаги обрезаем.
            if (actual != null && actual.getClass().toString().equals("class java.lang.String")) {
                String msg = (String) actual;
                int len = msg.length() < 255 ? msg.length() : 255;
                matcher.describeMismatch(msg.substring(0, len), description);
            } else {
                matcher.describeMismatch(actual, description);
            }
            errors.add(new AssertionException(description.toString()));
            if (LOG != null) {
                LOG.info("[Failed checking] {}\n", description);
            }
        }
    }

    public void assertThat(String reason, boolean assertion) {
        StringDescription description = new StringDescription();
        description.appendText(reason);
        if (!assertion) {
            description.appendText("\n      expected: <true>")
                    .appendText("\n        actual: <false>");
            errors.add(new AssertionException(description.toString()));
            if (LOG != null) {
                LOG.info("[Failed checking] {}\n", description);
            }
        } else {
            description.appendText("\n      expected: <true>")
                    .appendText("\n        actual: <true>");
            if (LOG != null) {
                LOG.info("[Successful checking] {}\n", description);
            }
        }
    }

    public void assertThat(String reason) {
        StringDescription description = new StringDescription();
        description.appendText(reason);
        errors.add(new AssertionException(description.toString()));
        if (LOG != null) {
            LOG.info("[Failed checking] {}", description);
        }
    }

    public void complete() {
        if(!errors.isEmpty()) {
            if(errors.size() == 1) {
                throw errors.get(0);
            } else {
                StringBuilder description = new StringBuilder();
                description.append(String.format("There were %d errors:", errors.size()));
                for (Throwable e : errors) {
                    description.append(String.format("%n  %s: %s", e.getClass().getName(), e.getMessage()));
                }

                throw new AssertionException(description.toString());
            }
        }
    }

    public static <T> void fastAssertThat(String reason, T actual, Matcher<? super T> matcher) {
        SoftlyAssert fast = new SoftlyAssert();
        fast.assertThat(reason, actual, matcher);
        fast.complete();
    }

}
