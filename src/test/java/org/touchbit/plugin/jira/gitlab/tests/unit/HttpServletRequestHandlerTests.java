/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.touchbit.plugin.jira.gitlab.utils.HttpServletRequestHandler;
import org.apache.commons.fileupload.FileItem;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("HttpServletRequestHandler tests")
class HttpServletRequestHandlerTests extends BaseTest {

    @Test
    void unitTest_20180421024051() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("action");
        when(item.getString()).thenReturn("test");
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(true));
        assertThat(handler.isActionSave(), is(false));
        assertThat(handler.getConfiguration(), is(nullValue()));
    }

    @Test
    void unitTest_20180421045957() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("action");
        when(item.getString()).thenReturn("save");
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(false));
        assertThat(handler.isActionSave(), is(true));
        assertThat(handler.getConfiguration(), is(nullValue()));
    }

    @Test
    void unitTest_20180421050050() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("action");
        when(item.getString()).thenReturn("other");
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(false));
        assertThat(handler.isActionSave(), is(false));
        assertThat(handler.getConfiguration(), is(nullValue()));
    }

    @Test
    void unitTest_20180421050140() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("other");
        when(item.getString()).thenReturn("test");
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(false));
        assertThat(handler.isActionSave(), is(false));
        assertThat(handler.getConfiguration(), is(nullValue()));
    }

    @Test
    void unitTest_20180421050215() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("configuration");
        when(item.getString()).thenReturn("{}");
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(false));
        assertThat(handler.isActionSave(), is(false));
        assertThat(handler.getConfiguration().toString(), is(new Configuration().toString()));
    }

    @Test
    void unitTest_20180421050728() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("configuration");
        when(item.getString()).thenReturn("test");
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(false));
        assertThat(handler.isActionSave(), is(false));
        assertThat(handler.getConfiguration(), is(nullValue()));
    }

    @Test
    void unitTest_20180421050757() throws Exception {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Method method = HttpServletRequestHandler.class.getDeclaredMethod("addItems", List.class);
        method.setAccessible(true);
        FileItem item = mock(FileItem.class);
        when(item.getFieldName()).thenReturn("configuration");
        when(item.getString()).thenReturn(null);
        List<FileItem> list = new ArrayList<>();
        list.add(item);
        method.invoke(handler, list);
        assertThat(handler.isActionTest(), is(false));
        assertThat(handler.isActionSave(), is(false));
        assertThat(handler.getConfiguration(), is(nullValue()));
    }

}
