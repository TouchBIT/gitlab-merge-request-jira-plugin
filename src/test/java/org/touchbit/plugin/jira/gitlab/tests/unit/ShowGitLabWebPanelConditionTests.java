/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.objects.IssueImpl;
import org.touchbit.plugin.jira.gitlab.objects.JiraProjectImpl;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.touchbit.plugin.jira.gitlab.web.ShowGitLabWebPanelCondition;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("ShowGitLabWebPanelCondition tests")
class ShowGitLabWebPanelConditionTests extends BaseTest {

    @Test
    void unitTest_20180421051511() {
        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        condition.init(new HashMap<>());
    }

    @Test
    void unitTest_20180421051551() {
        JLab.addGitlabProjects("KEY1", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn("KEY1");
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn("KEY1-123");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(true));
    }

    @Test
    void unitTest_20180421054609() {
        JLab.addGitlabProjects("KEY2", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn("KEY2");
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn("          KEY2-123        ");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(true));
    }

    @Test
    void unitTest_20180421054915() {
        JLab.addGitlabProjects("KEY3", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn("KEY3");
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn(null);

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(true));
    }

    @Test
    void unitTest_20180421054452() {
        JLab.addGitlabProjects("   KEY4   ", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn("KEY4");
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn("KEY4-123");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(false));
    }

    @Test
    void unitTest_20180421054536() {
        JLab.addGitlabProjects("KEY5", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn("     KEY5     ");
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn("KEY5-123");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(false));
    }

    @Test
    void unitTest_20180421054958() {
        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn("KEY6");
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn("KEY6-123");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(false));
    }

    @Test
    void unitTest_20180421055057() {
        JLab.addGitlabProjects("KEY7", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Project project = mock(JiraProjectImpl.class);
        when(project.getKey()).thenReturn(null);
        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(project);
        when(issue.getKey()).thenReturn("KEY7-123");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(false));
    }

    @Test
    void unitTest_20180421060146() {
        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", null);
        assertThat(condition.shouldDisplay(context), is(false));
    }

    @Test
    void unitTest_20180421060226() {
        JLab.addGitlabProjects("KEY8", new ArrayList<org.touchbit.plugin.jira.gitlab.model.gitlab.Project>() {{
            add(new org.touchbit.plugin.jira.gitlab.model.gitlab.Project());
        }});

        Issue issue = mock(IssueImpl.class);
        when(issue.getProjectObject()).thenReturn(null);
        when(issue.getKey()).thenReturn("KEY8-123");

        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", issue);
        assertThat(condition.shouldDisplay(context), is(false));
    }

    @Test
    void unitTest_20180421060256() {
        ShowGitLabWebPanelCondition condition = new ShowGitLabWebPanelCondition();
        Map<String, Object> context = new HashMap<>();
        context.put("issue", new ArrayList<>());
        assertThat(condition.shouldDisplay(context), is(false));
    }
}
