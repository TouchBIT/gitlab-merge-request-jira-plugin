/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import org.touchbit.plugin.jira.gitlab.model.gitlab.Assignee;
import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.touchbit.plugin.jira.gitlab.utils.SoftlyAssert;
import org.touchbit.plugin.jira.gitlab.model.jlab.Row;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.touchbit.plugin.jira.gitlab.model.jlab.MrWebStatus.CAN_BE_MERGED;
import static org.touchbit.plugin.jira.gitlab.model.jlab.MrWebStatus.OPENED_STATE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/**
 * Created by Oleg Shaburov on 04.05.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("Row tests")
class RowTests extends BaseTest {

    private static final String PATH_WITH_NAMESPACE = "name/space/name";
    private static final String PROJECT_WEB_URI = "http://gitlab.test:12345";
    private static final String TITLE = "Title";
    private static final String MR_WEB_URL = "WebUrl";
    private static final String SOURCE_BRANCH = "SourceBranch";
    private static final String TARGET_BRANCH = "TargetBranch";
    private static final Assignee ASSIGNEE = new Assignee();
    private static final String ASSIGNEE_NAME = "ASSIGNEE_NAME";
    private static final int UPVOTES = 0;
    private static final int DOWNVOTES = 0;

    static {
        ASSIGNEE.setName(ASSIGNEE_NAME);
    }

    @Test
    void unitTest_20180504020719() {
        Project project = new Project();
        project.setPathWithNamespace(PATH_WITH_NAMESPACE);
        project.setWebUrl(PROJECT_WEB_URI);
        MergeRequest request = new MergeRequest();
        request.setState(OPENED_STATE);
        request.setMergeStatus(CAN_BE_MERGED);
        request.setTitle(TITLE);
        request.setWebUrl(MR_WEB_URL);
        request.setSourceBranch(SOURCE_BRANCH);
        request.setTargetBranch(TARGET_BRANCH);
        request.setAssignee(ASSIGNEE);
        request.setUpvotes(UPVOTES);
        request.setDownvotes(DOWNVOTES);
        Row row = new Row(project, request);
        SoftlyAssert softlyAssert = new SoftlyAssert();
        softlyAssert.assertThat(row.getAssignee(), is(ASSIGNEE_NAME));
        softlyAssert.assertThat(row.getMrStatus(), is("REVIEW"));
        softlyAssert.assertThat(row.getMrTitle(), is(TITLE));
        softlyAssert.assertThat(row.getMrLink(), is(MR_WEB_URL));
        softlyAssert.assertThat(row.getMrProject(), is(PATH_WITH_NAMESPACE));
        softlyAssert.assertThat(row.getSource(), is(SOURCE_BRANCH));
        softlyAssert.assertThat(row.getSourceLink(), is(PROJECT_WEB_URI + "/commits/" + SOURCE_BRANCH));
        softlyAssert.assertThat(row.getTarget(), is(TARGET_BRANCH));
        softlyAssert.assertThat(row.getTargetLink(), is(PROJECT_WEB_URI + "/commits/" + TARGET_BRANCH));
        softlyAssert.assertThat(row.getUpVotes(), is(0));
        softlyAssert.assertThat(row.getDownVotes(), is(0));
        softlyAssert.complete();
    }

    @Test
    void unitTest_20180504023034() {
        Project project = new Project();
        project.setPathWithNamespace(PATH_WITH_NAMESPACE);
        project.setWebUrl(PROJECT_WEB_URI);
        MergeRequest request = new MergeRequest();
        request.setState(OPENED_STATE);
        request.setMergeStatus(CAN_BE_MERGED);
        request.setTitle(TITLE);
        request.setWebUrl(MR_WEB_URL);
        request.setSourceBranch(SOURCE_BRANCH);
        request.setTargetBranch(TARGET_BRANCH);
        request.setAssignee(ASSIGNEE);
        request.setUpvotes(UPVOTES);
        request.setDownvotes(DOWNVOTES);
        Row row = new Row(project, request);
        String msg = row.toString();
        assertThat(msg, not(nullValue()));
        assertThat(msg.length() > 0, is(true));
        assertThat(msg.contains("\n"), is(false));
        assertThat(msg.startsWith(" | "), is(true));
        assertThat(msg.endsWith(" | "), is(true));
    }

    @Test
    void unitTest_20180504023603() {
        Project project = new Project();
        project.setPathWithNamespace(PATH_WITH_NAMESPACE);
        project.setWebUrl(PROJECT_WEB_URI);
        MergeRequest request = new MergeRequest();
        request.setUpvotes(1);
        request.setDownvotes(1);
        Row row = new Row(project, request);
        SoftlyAssert softlyAssert = new SoftlyAssert();
        softlyAssert.assertThat(row.getUpVotes(), is("+1"));
        softlyAssert.assertThat(row.getDownVotes(), is("-1"));
        softlyAssert.complete();
    }

}
