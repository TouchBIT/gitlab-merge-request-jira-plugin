/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import org.touchbit.plugin.jira.gitlab.utils.JSON;
import org.touchbit.plugin.jira.gitlab.model.jlab.*;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

/**
 * Created by Oleg Shaburov on 20.04.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("ConstantConditions")
@DisplayName("Json tests")
class JsonTests extends BaseTest {

    @Test
    void unitTest_20180420162642() {
        String str = "   plugin error   ";
        Configuration configuration = new Configuration();
        configuration.setPluginError(str);
        assertThat(configuration.getPluginError().getMsg(), is(str));
    }

    @Test
    void unitTest_20180420164452() {
        Configuration configuration = new Configuration();
        PluginError error = new PluginError();
        configuration.setPluginError(error);
        assertThat(configuration.getPluginError(), is(error));
    }

    @Test
    void unitTest_20180420164619() {
        List<String> keys = new ArrayList<String>() {{add("   KEY   ");}};
        Configuration configuration = new Configuration();
        configuration.setJiraProjectKeys(keys);
        assertThat(configuration.getJiraProjectKeys(), is(keys));
    }

    @Test
    void unitTest_20180420164819() {
        String str = "   plugin admins    ";
        Configuration configuration = new Configuration();
        configuration.setGroupOfPluginJiraAdmins(str);
        assertThat(configuration.getGroupOfPluginJiraAdmins(), is(str));
    }

    @Test
    void unitTest_20180420164912() {
        List<Connection> connections = new ArrayList<Connection>() {{add(new Connection());}};
        Configuration configuration = new Configuration();
        configuration.setConnections(connections);
        assertThat(configuration.getConnections(), is(connections));
    }

    @Test
    void unitTest_20180420165242() {
        Configuration configuration = new Configuration();
        assertThat(configuration.getRelatedJiraProjectKeys(), is(new ArrayList<>()));
    }

    @Test
    void unitTest_20180420165430() {
        Relation relation = new Relation();
        relation.setJiraProjectKey("   KEY   ");
        Connection connection = new Connection();
        connection.setRelations(new ArrayList<Relation>() {{ add(relation); }});
        List<Connection> connections = new ArrayList<Connection>() {{ add(connection); }};
        Configuration configuration = new Configuration();
        configuration.setConnections(connections);
        assertThat(configuration.getRelatedJiraProjectKeys(), is(new ArrayList<String>() {{ add("   KEY   "); }}));
    }

    @Test
    void unitTest_20180420171053() {
        String str = "{\"jiraProjectKeys\":[\"ABC\"]}";
        Configuration configuration = JSON.map(str, Configuration.class);
        assertThat(configuration, notNullValue());
        assertThat(JSON.toString(configuration),
                is("{\"jiraProjectKeys\":[\"ABC\"],\"connections\":[],\"relatedJiraProjectKeys\":[]}"));
    }

    @Test
    void unitTest_20180420171524() {
        String str = "";
        Configuration configuration = JSON.map(str, Configuration.class);
        assertThat(configuration, nullValue());
    }

    @Test
    void unitTest_20180420171545() {
        Configuration configuration = JSON.map(null, Configuration.class);
        assertThat(configuration, nullValue());
    }

    @Test
    void unitTest_20180420170025() {
        List<Relation> relations = new ArrayList<Relation>() {{ add(new Relation()); }};
        Connection connection = new Connection();
        connection.setRelations(relations);
        assertThat(connection.getRelations(), is(relations));
    }

    @Test
    void unitTest_20180420170153() {
        Connection connection = new Connection();
        connection.setApiToken("   token   ");
        assertThat(connection.getApiToken(), is("   token   "));
    }

    @Test
    void unitTest_20180421222600() {
        Connection connection = new Connection();
        connection.setGitLabVersion("   version   ");
        assertThat(connection.getGitLabVersion(), is("   version   "));
    }

    @Test
    void unitTest_20180421222625() {
        Connection connection = new Connection();
        connection.setGitLabUser("   user   ");
        assertThat(connection.getGitLabUser(), is("   user   "));
    }

    @Test
    void unitTest_20180420170238() {
        Connection connection = new Connection();
        connection.setAddress("   https://gitlab.test:8080/   ");
        assertThat(connection.getAddress(), is("   https://gitlab.test:8080/   "));
    }

    @Test
    void unitTest_20180420170724() {
        Connection connection = new Connection();
        connection.setCheckGitLabResult("    result    ");
        assertThat(connection.getCheckGitLabResult(), is("    result    "));
    }

    @Test
    void unitTest_20180420191232() {
        Connection connection = new Connection();
        assertThat(connection.getCheckGitLabResult(), nullValue());
    }

    @Test
    void unitTest_20180420173614() {
        Relation relation = new Relation();
        relation.setJiraProjectKey("   KEY   ");
        assertThat(relation.getJiraProjectKey(), is("   KEY   "));
    }

    @Test
    void unitTest_20180420174039() {
        Relation relation = new Relation();
        relation.setJiraError("   ERROR    ");
        assertThat(relation.getJiraError(), is("   ERROR    "));
    }

    @Test
    void unitTest_20180420174430() {
        List<GlProject> projects = new ArrayList<GlProject>() {{ add(new GlProject()); }};
        Relation relation = new Relation();
        relation.setGitlabProjects(projects);
        assertThat(relation.getGitlabProjects(), is(projects));
    }
    
    @Test
    void unitTest_20180420174824() {
        GlProject project = new GlProject();
        project.setProjectError("  ERROR  ");
        assertThat(project.getProjectError(), is("  ERROR  "));
    }

    @Test
    void unitTest_20180420174944() {
        GlProject project = new GlProject();
        assertThat(project.getProjectError(), nullValue());
    }

    @Test
    void unitTest_20180420175004() {
        GlProject project = new GlProject();
        project.setProject("    namespace/name   ");
        assertThat(project.getProject(), is("    namespace/name   "));
    }

    @Test
    void unitTest_20180420175126() {
        GlProject project = new GlProject();
        assertThat(project.getProject(), nullValue());
    }

    @Test
    void unitTest_20180420175342() {
        GlProject project = new GlProject();
        project.setSearchLine("   CLOSES   ");
        assertThat(project.getSearchLine(), is("   CLOSES   "));
    }

    @Test
    void unitTest_20180420175440() {
        GlProject project = new GlProject();
        assertThat(project.getSearchLine(), nullValue());
    }

    @Test
    void unitTest_20180420175509() {
        GlProject project = new GlProject();
        project.setSearchLineError("   ERROR   ");
        assertThat(project.getSearchLineError(), is("   ERROR   "));
    }

    @Test
    void unitTest_20180420175731() {
        GlProject project = new GlProject();
        assertThat(project.getSearchLineError(), nullValue());
    }

    @Test
    void unitTest_20180420190813() {
        GlProject project = new GlProject();
        project.setProjectID(123);
        assertThat(project.getProjectID(), is(123));
    }

    @Test
    void unitTest_20180420191057() {
        GlProject project = new GlProject();
        assertThat(project.getProjectID(), nullValue());
    }

    @Test
    void unitTest_20180420190840() {
        GlProject project = new GlProject();
        project.setProjectPathName("  test_test.test  ");
        assertThat(project.getProjectPathName(), is("  test_test.test  "));
    }

    @Test
    void unitTest_20180420191117() {
        GlProject project = new GlProject();
        assertThat(project.getProjectPathName(), nullValue());
    }

    @Test
    void unitTest_20180420190937() {
        GlProject project = new GlProject();
        project.setProjectPathNameSpace("  name/space/with/subgroups  ");
        assertThat(project.getProjectPathNameSpace(), is("  name/space/with/subgroups  "));
    }

    @Test
    void unitTest_20180420191127() {
        GlProject project = new GlProject();
        assertThat(project.getProjectPathNameSpace(), nullValue());
    }

    @Test
    void unitTest_20180420191339() {
        PluginError pluginError = new PluginError();
        pluginError.setMsg("   MSG   ");
        assertThat(pluginError.getMsg(), is("   MSG   "));
    }

    @Test
    void unitTest_20180420191638() {
        PluginError pluginError = new PluginError();
        assertThat(pluginError.getMsg(), nullValue());
    }

    @Test
    void unitTest_20180420192511() {
        PluginError pluginError = new PluginError();
        pluginError.setMsg("MSG");
        assertThat(JSON.toString(pluginError), is("{\"msg\":\"MSG\"}"));
    }

    @Test
    void unitTest_20180420192600() {
        PluginError pluginError = new PluginError();
        pluginError.setMsg("MSG");
        assertThat(JSON.toPrettyPrintString(pluginError), is("{\n  \"msg\" : \"MSG\"\n}"));
    }

    @Test
    void unitTest_20180420193551() {
        PluginError pluginError1 = new PluginError();
        pluginError1.setMsg(" MSG ");
        PluginError pluginError2 = new PluginError();
        pluginError2.setMsg("MSG");
        assertThat(pluginError1, not(equalTo(pluginError2)));
    }
}
