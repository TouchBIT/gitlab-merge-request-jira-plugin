/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.touchbit.plugin.jira.gitlab.servlets.CacheServlet;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

/**
 * Created by Oleg Shaburov on 04.05.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings({"unchecked", "WeakerAccess"})
@DisplayName("BaseServlet tests")
public class BaseServletTests extends BaseTest {

    @Test
    @DisplayName("Verifying the 'configToMap' method call in the CacheServlet class")
    void unitTest_20180504172920() throws Exception {
        UserManager userManager = getMockUserManager();
        LoginUriProvider uriProvider = getMockLoginUriProvider();
        TemplateRenderer templateRenderer = getMockTemplateRenderer();
        PluginSettingsFactory pluginSettingsFactory = getMockPluginSettingsFactory();
        CacheServlet servlet = new CacheServlet(userManager, uriProvider, templateRenderer, pluginSettingsFactory);
        Method method = servlet.getClass().getDeclaredMethod("configToMap");
        method.setAccessible(true);
        Map<String, Object> context = (Map<String, Object>) method.invoke(servlet);
        assertThat(context, not(nullValue()));
        assertThat(context.isEmpty(), is(false));
        assertThat(context.size(), is(1));
    }

}
