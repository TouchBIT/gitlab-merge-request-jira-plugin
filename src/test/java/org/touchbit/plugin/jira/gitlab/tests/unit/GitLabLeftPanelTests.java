/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.project.Project;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.objects.IssueImpl;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.touchbit.plugin.jira.gitlab.web.GitLabLeftPanel;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("GitLabLeftPanel tests")
class GitLabLeftPanelTests extends BaseTest {

    @Test
    @DisplayName("Success exp. -> 'init' method call with empty context")
    void unitTest_20180421182545() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, String> ctx = new HashMap<>();
        panel.init(ctx);
        assertThat(ctx, is(new HashMap<>()));
    }

    @Test
    @DisplayName("Success exp. -> 'getUniqueContextKey' method call with empty context")
    void unitTest_20180503225202() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        String key = panel.getUniqueContextKey(ctx);
        assertThat(key, is("issueMergeRequest:" + panel.toString()));
    }

    @Test
    @DisplayName("Success exp. -> 'getUniqueContextKey' method call if Issue have null values")
    void unitTest_20180503225349() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        ctx.put("issue", new IssueImpl());
        String key = panel.getUniqueContextKey(ctx);
        assertThat(key, is("issueMergeRequest:" + panel.toString()));
    }

    @Test
    @DisplayName("Success exp. -> 'getUniqueContextKey' method call if Issue contain key value")
    void unitTest_20180503225545() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("KEY-123");
        ctx.put("issue", issue);
        String key = panel.getUniqueContextKey(ctx);
        assertThat(key, is("issueMergeRequest:" + panel.toString()));
    }

    @Test
    @DisplayName("Success exp. -> 'getUniqueContextKey' method call if Issue contain key value Project contains null values")
    void unitTest_20180503225740() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Issue issue = mock(Issue.class);
        Project project = mock(Project.class);
        when(issue.getKey()).thenReturn("KEY-123");
        when(issue.getProjectObject()).thenReturn(project);
        ctx.put("issue", issue);
        String key = panel.getUniqueContextKey(ctx);
        assertThat(key, is("issueMergeRequest:KEY-123:null:null"));
    }

    @Test
    @DisplayName("Success exp. -> 'getUniqueContextKey' method call if Issue && Project contain key value")
    void unitTest_20180503225848() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Project project = mock(Project.class);
        when(project.getKey()).thenReturn("KEY");
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("KEY-123");
        when(issue.getProjectObject()).thenReturn(project);
        ctx.put("issue", issue);
        String key = panel.getUniqueContextKey(ctx);
        assertThat(key, is("issueMergeRequest:KEY-123:KEY:null"));
    }

    @Test
    @DisplayName("Success exp. -> 'getUniqueContextKey' method call if Issue && Project contain values")
    void unitTest_20180503230445() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Project project = mock(Project.class);
        when(project.getKey()).thenReturn("KEY");
        when(project.getName()).thenReturn("NAME");
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("KEY-123");
        when(issue.getProjectObject()).thenReturn(project);
        ctx.put("issue", issue);
        String key = panel.getUniqueContextKey(ctx);
        assertThat(key, is("issueMergeRequest:KEY-123:KEY:NAME"));
    }

    @Test
    @DisplayName("Success exp. -> 'getContextMap' method call return empty context Map")
    void unitTest_20180503230832() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Map<String, Object> contextMap = panel.getContextMap(ctx);
        assertThat(contextMap.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Success exp. -> 'getContextMap' method call return empty context Map if input context = null")
    void unitTest_20180506184439() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> contextMap = panel.getContextMap(null);
        assertThat(contextMap.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Success exp. -> 'getContextMap' method call return empty context Map if input Issue.key = null")
    void unitTest_20180503230857() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Issue issue = mock(Issue.class);
        ctx.put("issue", issue);
        Map<String, Object> contextMap = panel.getContextMap(ctx);
        assertThat(contextMap.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Success exp. -> 'getContextMap' method call return empty context Map if input Issue.key != null")
    void unitTest_20180503231041() {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("KEY");
        ctx.put("issue", issue);
        Map<String, Object> contextMap = panel.getContextMap(ctx);
        assertThat(contextMap.isEmpty(), is(true));
    }

    @Test
    @DisplayName("Success exp. -> 'getContextMap' method call return empty 'mrtable' " +
            "if input Issue.key != null && Issue.project != null")
    void unitTest_20180503231135() {
        JLab.clearCache();
        JLab.setConfiguration(null);
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Map<String, Object> ctx = new HashMap<>();
        Project project = mock(Project.class);
        Issue issue = mock(Issue.class);
        when(issue.getKey()).thenReturn("KEY");
        when(issue.getProjectObject()).thenReturn(project);
        ctx.put("issue", issue);
        Map<String, Object> contextMap = panel.getContextMap(ctx);
        assertThat(contextMap.isEmpty(), is(false));
        assertThat(contextMap.get("mrtable"), is(new ArrayList<>()));
    }

    @Test
    @DisplayName("Success exp. -> 'isCorrectIssue' method call return boolean value")
    void unitTest_20180503235302() throws Exception {
        GitLabLeftPanel panel = new GitLabLeftPanel();
        Method method = panel.getClass().getDeclaredMethod("isCorrectIssue", Issue.class);
        method.setAccessible(true);
        Issue issue = mock(Issue.class);
        assertThat(method.invoke(panel, issue), is(false));
    }
}
