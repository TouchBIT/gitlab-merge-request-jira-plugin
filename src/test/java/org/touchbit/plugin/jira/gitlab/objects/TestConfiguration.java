/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.objects;

import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.touchbit.plugin.jira.gitlab.model.jlab.Connection;
import org.touchbit.plugin.jira.gitlab.model.jlab.GlProject;
import org.touchbit.plugin.jira.gitlab.model.jlab.Relation;

import java.util.ArrayList;

import static org.touchbit.plugin.jira.gitlab.utils.Randomizer.getRandomString;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("ALL")
public class TestConfiguration extends Configuration {

    public static final Integer PROJECT_ID = 123;
    public static final String PROJECT_NAME = "namespase/test";
    public static final String GITLAB_HOST = "http://localhost:18181";
    public static final String TEST_JIRA_KEY_1 = "TEST1";
    public static final String TEST_JIRA_KEY_2 = "TEST2";

    public TestConfiguration() {
        setGroupOfPluginJiraAdmins(getRandomString(10));
        setJiraProjectKeys(new ArrayList<String>() {{ add(TEST_JIRA_KEY_1); add(TEST_JIRA_KEY_2); }});
        setConnections(new ArrayList<Connection>() {{ add(new TestConnection()); }});
    }

    public static class TestConnection extends Connection {

        public TestConnection() {
            setAddress(GITLAB_HOST);
            setApiToken(getRandomString(10));
            setRelations(new ArrayList<Relation>() {{
                add(new TestRelation(TEST_JIRA_KEY_1));
                add(new TestRelation(TEST_JIRA_KEY_2));
            }});
        }

        public static class TestRelation extends Relation {

            public TestRelation(String jiraKey) {
                setJiraProjectKey(jiraKey);
                setGitlabProjects(new ArrayList<GlProject>() {{
                    add(new TestGlProject(PROJECT_NAME)); add(new TestGlProject(PROJECT_ID));
                }});
            }

            public static class TestGlProject extends GlProject {

                public TestGlProject(String namespaceName) {
                    setProject(namespaceName);
                    setSearchLine(getRandomString(5));
                }

                public TestGlProject(Integer id) {
                    setProject(String.valueOf(id));
                    setSearchLine(getRandomString(5));
                }
            }
        }
    }
}