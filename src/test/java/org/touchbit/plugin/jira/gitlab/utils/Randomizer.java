/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
public class Randomizer {

    private static final String UPPER_LETTERS = "QWERTYUIOPASDFGHJKLZXCVBNM";
    private static final String LOWER_LETTERS = "qwertyuiopasdfghjklzxcvbnm";
    private static final String LETTERS = UPPER_LETTERS + LOWER_LETTERS;

    public static List<Project> getRandomGitlabProjects(int c) {
        List<Project> projects = new ArrayList<>();
        for (int i = 0; i < c; i++) {
            projects.add(getRandomGitlabProject());
        }
        return projects;
    }

    private static Project getRandomGitlabProject() {
        Project project = new Project();
            project.setId(getRandomInt(5));
            String nameSpace = getRandomString(5);
            String name = getRandomString(5);
            project.setPathWithNamespace(nameSpace + "/" + name);
            project.setPath(name);
            return project;
    }

    public static String getRandomString(int length) {
        return getRandomString(length, false);
    }

    public static String getRandomString(int length, boolean withLowerCase) {
        String str = generateString(LETTERS, length);
        return withLowerCase ? str.toLowerCase() : str;
    }

    public static Integer getRandomInt(int len) {
        StringBuilder strInt = new StringBuilder();
        for (int i = 0; i < len; i++) {
            strInt.append(getRandomIntBetween(1, 9));
        }
        if (strInt.length() > 0) {
            return Integer.valueOf(strInt.toString());
        }
        throw new IllegalArgumentException("The length of the generated number must be positive. Received: " + len);
    }

    public static Integer getRandomIntBetween(int min, int max) {
        if (min > max) {
            throw new IllegalArgumentException("The value of the minimum value in the range of the generated number " +
                    "can not be greater than the maximum value");
        }
        Random random = new Random();
        return  random.nextInt((max - min) + 1) + min;
    }

    private static String generateString(String characters, int length) {
        if (length < 1) {
            throw new IllegalArgumentException("The length of the generated string must be positive");
        }
        char[] text = new char[length];
        for (int i = 0; i < length; i++) {
            text[i] = characters.charAt(new Random().nextInt(characters.length()));
        }
        return new String(text);
    }

}
