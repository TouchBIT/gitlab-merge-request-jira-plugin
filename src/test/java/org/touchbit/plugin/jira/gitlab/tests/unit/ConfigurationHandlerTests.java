/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import com.atlassian.jira.security.groups.GroupManager;
import org.touchbit.plugin.jira.gitlab.model.jlab.Connection;
import org.touchbit.plugin.jira.gitlab.model.jlab.GlProject;
import org.touchbit.plugin.jira.gitlab.model.jlab.Relation;
import org.touchbit.plugin.jira.gitlab.objects.TestConfiguration;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.touchbit.plugin.jira.gitlab.utils.ConfigurationHandler;
import org.touchbit.plugin.jira.gitlab.utils.SoftlyAssert;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import ru.lanwen.wiremock.ext.WiremockResolver;
import ru.lanwen.wiremock.ext.WiremockUriResolver;

import static org.touchbit.plugin.jira.gitlab.objects.TestConfiguration.*;
import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * {@link ConfigurationHandler} class functional test
 * <p>
 * Created by Oleg Shaburov on 15.04.2018
 * shaburov.o.a@gmail.com
 */
@ExtendWith({
        WiremockResolver.class,
        WiremockUriResolver.class
})
@DisplayName("ConfigurationHandler tests")
class ConfigurationHandlerTests extends BaseTest {

    private static final String API_NAMESPACE = "/api/v4";

    private static final ResponseDefinitionBuilder VERSION_RESPONSE = aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody("{\"version\":\"8.13.0-pre\",\"revision\":\"4e963fe\"}");
    private static final ResponseDefinitionBuilder USER_RESPONSE = aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody("{\"name\":\"test.user\"}");
    private static final ResponseDefinitionBuilder PROJECT_RESPONSE = aResponse()
            .withHeader("Content-Type", "application/json")
            .withBody("{\"path_with_namespace\":\"" + PROJECT_NAME + "\",\"id\":\"" + PROJECT_ID + "\"}");

    @Test
    @Disabled("Fix Wiremock server")
    @DisplayName("ConfigurationHandler 'check' method call")
    void unitTest_20180415192258(@WiremockResolver.Wiremock WireMockServer server,
                                 @WiremockUriResolver.WiremockUri String uri) {
        stubFor(get(urlEqualTo(API_NAMESPACE + "/version")).willReturn(aResponse()
                .withBody(VERSION_RESPONSE.build().getBody()).withFixedDelay(500)));
        stubFor(get(urlEqualTo(API_NAMESPACE + "/user")).willReturn(USER_RESPONSE));
        stubFor(get(urlEqualTo(API_NAMESPACE + "/projects/" + PROJECT_NAME.replace("/", "%2F")))
                .willReturn(PROJECT_RESPONSE));
        stubFor(get(urlEqualTo(API_NAMESPACE + "/projects/" + PROJECT_ID)).willReturn(PROJECT_RESPONSE));
        GroupManager groupManager = mock(GroupManager.class);
        TestConfiguration configuration = new TestConfiguration();
        configuration.getConnections().forEach(c -> c.setAddress(uri));
        when(groupManager.groupExists(configuration.getGroupOfPluginJiraAdmins())).thenReturn(true);
        ConfigurationHandler.check(configuration, groupManager);
        SoftlyAssert softlyAssert = new SoftlyAssert();
        softlyAssert.assertThat("Check plugin admin group nod null",
                configuration.getGroupOfPluginJiraAdmins(), notNullValue());
        softlyAssert.assertThat("Check jira keys not null",
                configuration.getJiraProjectKeys(), notNullValue());
        softlyAssert.assertThat("Check gitlab connections not null",
                configuration.getConnections(), notNullValue());
        softlyAssert.assertThat("There are no errors in the [PluginError] if the configuration is filled correctly",
                configuration.getPluginError(), nullValue());
        for (Connection connection : configuration.getConnections()) {
            softlyAssert.assertThat("Check gitlab host not null", connection.getAddress(), notNullValue());
            softlyAssert.assertThat("Check token not null", connection.getApiToken(), notNullValue());
            softlyAssert.assertThat("Check relations not null", connection.getRelations(), notNullValue());
            softlyAssert.assertThat("Check gitlab version", connection.getCheckGitLabResult(), notNullValue());
            for (Relation relation : connection.getRelations()) {
                softlyAssert.assertThat("Check gitlab projects not null",
                        relation.getGitlabProjects(), notNullValue());
                softlyAssert.assertThat("Check jira project keys not null",
                        relation.getJiraProjectKey(), notNullValue());
                softlyAssert.assertThat("Check jira error is null", relation.getJiraError(), nullValue());
                for (GlProject glProject : relation.getGitlabProjects()) {
                    softlyAssert.assertThat("Check project not null", glProject.getProject(), notNullValue());
                    softlyAssert.assertThat("Check search line not null", glProject.getSearchLine(), notNullValue());
                    softlyAssert.assertThat("Check project error is null", glProject.getProjectError(), nullValue());
                    softlyAssert.assertThat("Check search line error is null", glProject.getSearchLineError(), nullValue());
                }
            }
        }
        softlyAssert.complete();
    }

}
