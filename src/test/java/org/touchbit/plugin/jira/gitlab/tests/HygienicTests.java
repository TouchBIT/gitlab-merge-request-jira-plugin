/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.touchbit.plugin.jira.gitlab.utils.SoftlyAssert;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

/**
 * Created by Oleg Shaburov on 04.05.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("ConstantConditions")
@DisplayName("Hygienic tests")
class HygienicTests extends BaseTest {

    private static final String TEST_DIR = "src/test/java/org/touchbit/plugin/jira/gitlab/tests";
    private static final ClassLoader CLASS_LOADER = HygienicTests.class.getClassLoader();

    @Test
    @DisplayName("Check unit test class names ends with 'Tests.java'")
    void unitTest_20180504003914() {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        SoftlyAssert softlyAssert = new SoftlyAssert();
        for (File file : testDirs) {
            if (!file.isDirectory() && !file.getName().equals("BaseTest.java")) {
                softlyAssert.assertThat(file.getName() + " ends with Tests.java",
                        file.getName().endsWith("Tests.java"), is(true));
            }
        }
        softlyAssert.complete();
    }

    @Test
    @DisplayName("Check unit test classes extends BaseTest")
    void unitTest_20180504005235() throws Exception {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        List<Class<?>> classList = new ArrayList<>();
        for (File dir : testDirs) {
            classList.addAll(getTestCalsses(dir));
        }
        SoftlyAssert softlyAssert = new SoftlyAssert();
        for (Class<?> aClass : classList) {
            if (!aClass.getName().equals("BaseTest.java")) {
                softlyAssert.assertThat(aClass.getName() + " extends BaseTest",
                        BaseTest.class.isAssignableFrom(aClass), is(true));
            }
        }
        softlyAssert.complete();
    }

    @Test
    @DisplayName("Check unit test class methods starts with 'unitTest_' prefix")
    void unitTest_20180504005636() throws Exception {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        List<Class<?>> classList = new ArrayList<>();
        for (File dir : testDirs) {
            classList.addAll(getTestCalsses(dir));
        }
        SoftlyAssert softlyAssert = new SoftlyAssert();
        for (Class<?> aClass : classList) {
            if (!aClass.getName().equals("BaseTest.java")) {
                for (Method method : aClass.getDeclaredMethods()) {
                    if (!method.getName().contains("jacocoInit") && method.isAnnotationPresent(org.junit.jupiter.api.Test.class)) {
                        if (method.getName().startsWith("lambda")) {
                            softlyAssert.assertThat(method.getName() + " starts with [lambda$unitTest_]",
                                    method.getName().startsWith("lambda$unitTest_"), is(true));
                        } else {
                            softlyAssert.assertThat(method.getName() + " starts with [unitTest_]",
                                    method.getName().startsWith("unitTest_"), is(true));
                        }
                    }
                }
            }
        }
        softlyAssert.complete();
    }

    @Test
    @DisplayName(value = "Check test classes contains @DisplayName annotation")
    void unitTest_20180506140412() throws Exception {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        SoftlyAssert softlyAssert = new SoftlyAssert();
        List<Class<?>> classList = new ArrayList<>();
        for (File dir : testDirs) {
            classList.addAll(getTestCalsses(dir));
        }
        classList.forEach(c -> softlyAssert.assertThat(c.getSimpleName() + " contains @DisplayName",
                c.getAnnotation(DisplayName.class), not(nullValue())));
        softlyAssert.complete();
    }

    @Test
    @DisplayName(value = "Check @DisplayName annotation values is not empty")
    void unitTest_20180506161057() throws Exception {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        List<Class<?>> classList = new ArrayList<>();
        for (File dir : testDirs) {
            classList.addAll(getTestCalsses(dir));
        }
        SoftlyAssert softlyAssert = new SoftlyAssert();
        classList.forEach(c -> {
            DisplayName actDisplayName = c.getAnnotation(DisplayName.class);
            if (actDisplayName != null) {
                softlyAssert.assertThat(c.getSimpleName() + ".class @DisplayName value",
                        actDisplayName.value(), not(""));
            }
        });
        softlyAssert.complete();
    }

    @Test
    @DisplayName(value = "Check test methods contains @DisplayName annotation")
    @Disabled
    void unitTest_20180506180615() throws Exception {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        SoftlyAssert softlyAssert = new SoftlyAssert();
        List<Class<?>> classList = new ArrayList<>();
        for (File dir : testDirs) {
            classList.addAll(getTestCalsses(dir));
        }
        for (Class<?> aClass : classList) {
            for (Method method : aClass.getDeclaredMethods()) {
                if (method.getName().startsWith("unitTest_")) {
                    softlyAssert.assertThat(method.getName() + " contains @DisplayName",
                            method.getAnnotation(DisplayName.class), not(nullValue()));
                }
            }
        }
        softlyAssert.complete();
    }

    @Test
    @DisplayName(value = "Check test methods @DisplayName annotation value is not empty")
    void unitTest_20180506181012() throws Exception {
        List<File> testDirs = getAllTestDirs();
        assertThat(testDirs, not(empty()));
        SoftlyAssert softlyAssert = new SoftlyAssert();
        List<Class<?>> classList = new ArrayList<>();
        for (File dir : testDirs) {
            classList.addAll(getTestCalsses(dir));
        }
        for (Class<?> aClass : classList) {
            for (Method method : aClass.getDeclaredMethods()) {
                if (method.getName().startsWith("unitTest_") && method.isAnnotationPresent(DisplayName.class)) {
                    softlyAssert.assertThat(method.getName() + " @DisplayName value",
                            method.getAnnotation(DisplayName.class).value(), not(""));
                }
            }
        }
        softlyAssert.complete();
    }

    private List<Class<?>> getTestCalsses(File f) throws Exception {
        List<Class<?>> classList = new ArrayList<>();
        for (File file : f.listFiles()) {
            if (file.isFile() && file.getName().contains("Tests")) {
                classList.add(CLASS_LOADER.loadClass(file.getPath()
                        .replace(File.separator, ".")
                        .replace("src.test.java.", "")
                        .replace(".java", "")));
            }
        }
        return classList;
    }

    private List<File> getAllTestDirs() {
        List<File> dirs = new ArrayList<>();
        File tetDir = new File(TEST_DIR);
        if (tetDir.isDirectory()) {
            dirs.add(tetDir);
            dirs.addAll(getDirs(tetDir));
        } else {
            throw new RuntimeException(TEST_DIR + " is not a directory");
        }
        return dirs;
    }

    private List<File> getDirs(File file) {
        List<File> dirs = new ArrayList<>();
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                if (f.isDirectory()) {
                    dirs.add(f);
                    dirs.addAll(getDirs(f));
                }
            }
        }
        return dirs;
    }

}
