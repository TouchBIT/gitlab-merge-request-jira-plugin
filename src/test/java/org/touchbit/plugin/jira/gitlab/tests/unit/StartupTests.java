/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.tests.unit;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.component.Startup;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.touchbit.plugin.jira.gitlab.tests.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.touchbit.plugin.jira.gitlab.utils.JSON;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by Oleg Shaburov on 20.04.2018
 * shaburov.o.a@gmail.com
 */
@DisplayName("Startup tests")
class StartupTests extends BaseTest {

    @Test
    void unitTest_20180420214457() {
        JLab.clearCache();
        Configuration config = new Configuration();
        config.setJiraProjectKeys(new ArrayList<String>() {{ add("ABC"); }});
        String storage = "org.touchbit.plugin.jira.gitlab.config";
        String configuration = "configuration";
        PluginSettings settings = mock(PluginSettings.class);
        when(settings.get(configuration)).thenReturn("");
        PluginSettingsFactory factory = mock(PluginSettingsFactory.class);
        when(factory.createSettingsForKey(storage)).thenReturn(settings);
        Startup startup = new Startup(factory);
        startup.onStart();
        assertThat(JLab.isInitialized(), is(false));
        assertThat(JLab.getConfiguration(), nullValue());

        settings = mock(PluginSettings.class);
        when(settings.get(configuration)).thenReturn("{\"jiraProjectKeys\":[\"ABC\"]}");
        factory = mock(PluginSettingsFactory.class);
        when(factory.createSettingsForKey(storage)).thenReturn(settings);
        startup = new Startup(factory);
        startup.onStart();
        assertThat(JLab.isInitialized(), is(true));
        assertThat(JSON.toString(JLab.getConfiguration()), is(JSON.toString(config)));

        settings = mock(PluginSettings.class);
        when(settings.get(configuration)).thenReturn(null);
        factory = mock(PluginSettingsFactory.class);
        when(factory.createSettingsForKey(storage)).thenReturn(settings);
        startup = new Startup(factory);
        startup.onStart();
        assertThat(JLab.isInitialized(), is(false));
        assertThat(JLab.getConfiguration(), is(nullValue()));
    }

}
