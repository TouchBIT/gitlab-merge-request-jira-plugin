/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.jlab;

import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.model.jlab.MrWebStatus;
import org.jetbrains.annotations.NotNull;

import java.util.StringJoiner;

/**
 * The row of the table with merge request information
 *
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
@SuppressWarnings("unused")
public class Row {

    private final Object mrLink;
    private final Object mrTitle;
    private final Object mrProject;
    private final Object mrStatus;
    private final Object source;
    private final Object sourceLink;
    private final Object target;
    private final Object targetLink;
    private final Object assignee;
    private final Object uv;
    private final Object dv;

    public Row(@NotNull final Project project, @NotNull final MergeRequest mr) {
        mrStatus = MrWebStatus.get(mr);
        mrTitle = mr.getTitle();
        mrLink = mr.getWebUrl();
        source = mr.getSourceBranch();
        target = mr.getTargetBranch();
        assignee = mr.getAssignee() == null ? "" : mr.getAssignee().getName();
        uv = mr.getUpvotes()   == 0 ? 0 : "+" + mr.getUpvotes();
        dv = mr.getDownvotes() == 0 ? 0 : "-" + mr.getDownvotes();
        mrProject = project.getPathWithNamespace();
        sourceLink = project.getWebUrl() + "/commits/" + mr.getSourceBranch();
        targetLink = project.getWebUrl() + "/commits/" + mr.getTargetBranch();
    }

    public Object getSourceLink() {
        return sourceLink;
    }

    public Object getTargetLink() {
        return targetLink;
    }

    public Object getSource() {
        return source;
    }

    public Object getTarget() {
        return target;
    }

    public Object getAssignee() {
        return assignee;
    }

    public Object getUpVotes() {
        return uv;
    }

    public Object getDownVotes() {
        return dv;
    }

    public Object getMrLink() {
        return mrLink;
    }

    public Object getMrTitle() {
        return mrTitle;
    }

    public Object getMrStatus() {
        return mrStatus;
    }

    public Object getMrProject() {
        return mrProject;
    }

    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(" | ", " | ", " | ");
        sj.add(String.valueOf(mrLink));
        sj.add(String.valueOf(mrTitle));
        sj.add(String.valueOf(mrProject));
        sj.add(String.valueOf(source));
        sj.add(String.valueOf(sourceLink));
        sj.add(String.valueOf(target));
        sj.add(String.valueOf(targetLink));
        sj.add(String.valueOf(assignee));
        sj.add(String.valueOf(mrStatus));
        sj.add(String.valueOf(uv));
        sj.add(String.valueOf(dv));
        return sj.toString();
    }
}