/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.jlab;

import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;

/**
 * Statuses of merge requests.
 *
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
public enum MrWebStatus {

    WIP,
    CONFLICT,
    REVIEW,
    MERGED,
    CLOSED,
    ERROR,
    ;

    public static final String OPENED_STATE = "opened";
    public static final String MERGED_STATE = "merged";
    public static final String CLOSED_STATE = "closed";
    public static final String CAN_BE_MERGED = "can_be_merged";

    public static String get(final MergeRequest mr) {
        try {
            switch (mr.getState()) {
                case OPENED_STATE:
                        boolean wip = mr.getTitle().substring(0, 3).contains("WIP");
                        if (!CAN_BE_MERGED.equalsIgnoreCase(mr.getMergeStatus()) && !wip) {
                            return CONFLICT.name();
                        }
                        if (wip) {
                            mr.setTitle(mr.getTitle().replace("WIP:", "").trim());
                            return WIP.name();
                        }
                        return REVIEW.name();
                case MERGED_STATE:
                    return MERGED.name();
                case CLOSED_STATE:
                    return CLOSED.name();
                default:
                    return mr.getState();
            }
        } catch (Exception ignore) {
            return ERROR.name();
        }
    }

}
