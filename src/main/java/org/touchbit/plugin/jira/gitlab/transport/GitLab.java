/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.transport;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Version;
import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.model.gitlab.User;

import java.util.List;

/**
 * Created by Oleg Shaburov on 24.06.2018
 * shaburov.o.a@gmail.com
 */
@Headers("Content-Type: application/json")
public interface GitLab {

    @RequestLine(value = "GET /projects/{projectId}/merge_requests?search={search}", decodeSlash = false)
    List<MergeRequest> searchProjectMergeRequests(@Param(value = "projectId") String projectId,@Param("search") String search);

    @RequestLine(value = "GET /user", decodeSlash = false)
    User getUser();

    @RequestLine(value = "GET /version", decodeSlash = false)
    Version getVersion();

    @RequestLine(value = "GET /projects/{projectId}", decodeSlash = false)
    Project getProject(@Param(value = "projectId") String projectId);

}
