/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.web;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.plugin.webfragment.CacheableContextProvider;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.model.jlab.Row;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Implementation of a web panel with a list of Merge Requests for the current issue.
 *
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
public class GitLabLeftPanel implements CacheableContextProvider {

    private static final Logger LOG = LoggerFactory.getLogger(ShowGitLabWebPanelCondition.class);

    static final String ISSUE = "issue";

    @Override
    public void init(Map<String, String> map) {
        //do nothing
    }

    @Override
    public String getUniqueContextKey(Map<String, Object> context) {
        final Issue issue = (Issue) context.get(ISSUE);
        StringJoiner sj = new StringJoiner(":", "issueMergeRequest:" ,"");
        if (issue != null && issue.getProjectObject() != null) {
            sj.add(issue.getKey());
            sj.add(issue.getProjectObject().getKey());
            sj.add(issue.getProjectObject().getName());
        } else {
            sj.add(this.toString());
        }
        return sj.toString();
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        Map<String, Object> resultContext = new HashMap<>();
        if (context != null) {
            final Issue issue = (Issue) context.get(ISSUE);
            List<Row> table = new ArrayList<>();
            if (isCorrectIssue(issue)) {
                LOG.info("Create merge requests table for {}", issue.getKey());
                @SuppressWarnings("ConstantConditions")
                final String jiraProjectKey = issue.getProjectObject().getKey();
                final String jiraIssueKey = issue.getKey();
                Map<Project, List<MergeRequest>> mergeRequests = JLab.searchMRs(jiraProjectKey, jiraIssueKey);
                mergeRequests.forEach((p, mrs) -> mrs.forEach(mr -> table.add(new Row(p, mr))));
                if (LOG.isDebugEnabled()) {
                    StringJoiner sj = new StringJoiner("\n");
                    table.forEach(t -> sj.add(t.toString()));
                    LOG.debug("The merge requests table was generated\n{}", sj);
                }
                resultContext.put("mrtable", table);
            }
        }
        return resultContext;
    }

    private boolean isCorrectIssue(Issue issue) {
        return issue !=null && issue.getKey() != null && issue.getProjectObject() != null;
    }

}
