/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.servlets;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.touchbit.plugin.jira.gitlab.utils.JSON;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.touchbit.plugin.jira.gitlab.utils.ConfigurationHandler;
import org.touchbit.plugin.jira.gitlab.utils.HttpServletRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static org.touchbit.plugin.jira.gitlab.utils.HttpServletRequestHandler.CONFIGURATION;

/**
 * Configuration plugin servlet.
 *
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
public class AdminServlet extends BaseServlet {

    private static final Logger LOG = LoggerFactory.getLogger(AdminServlet.class);

    private final List<String> listJiraProjectKeys = new ArrayList<>();

    public AdminServlet(@ComponentImport final UserManager userManager,
                        @ComponentImport final LoginUriProvider loginUriProvider,
                        @ComponentImport final TemplateRenderer renderer,
                        @ComponentImport final PluginSettingsFactory factory) {
        super(userManager, loginUriProvider, renderer, factory);
        final ProjectManager projectManager = ComponentAccessor.getOSGiComponentInstanceOfType(ProjectManager.class);
        final Collection<Project> projects = projectManager.getProjectObjects();
        final List<String> keys = projects.stream().map(Project::getKey).collect(Collectors.toList());
        setJiraProjectKeys(keys);
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
        LOG.info("GET method call");
        Map<String, Object> map = new HashMap<>();
        LOG.debug("Get settings");
        Configuration configuration = getSavedConfiguration();
        if (userHasAccess(request, response, configuration)) {
            LOG.debug("Set existing jira project keys");
            configuration.setJiraProjectKeys(listJiraProjectKeys);
            ConfigurationHandler.check(configuration, getGroupManager());
            String json = JSON.toString(configuration);
            map.put(CONFIGURATION, json);
            LOG.debug("Settings: {}", json);
            renderAdmin(map, response);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
        LOG.info("POST method call");
        HttpServletRequestHandler handler = new HttpServletRequestHandler(request);
        Map<String, Object> map = new HashMap<>();
        Configuration configuration = handler.getConfiguration();
        if (configuration == null) {
            try {
                response.sendError(400, "Field [" + CONFIGURATION + "] is missing");
            } catch (IOException e) {
                LOG.error("Error occurred while try send 400 error code", e);
            }
            return;
        }
        if (userHasAccess(request, response, configuration)) {
            if (LOG.isDebugEnabled()) {
                LOG.debug("Pretty print received request body:: \n{}", JSON.toPrettyPrintString(configuration));
            }
            if (handler.isActionTest() && ConfigurationHandler.check(configuration, getGroupManager())) {
                LOG.info("The configuration is correct.");
            }
            if (handler.isActionSave() && ConfigurationHandler.check(configuration, getGroupManager())) {
                saveConfiguration(configuration);
                LOG.info("The configuration is saved successfully.");
            }
            LOG.debug("Add all available jira project keys to response");
            configuration.setJiraProjectKeys(listJiraProjectKeys);

            map.put(CONFIGURATION, JSON.toString(configuration));
            if (LOG.isDebugEnabled()) {
                LOG.debug("Pretty print response body:\n{}", JSON.toPrettyPrintString(configuration));
            }
            renderAdmin(map, response);
        }
    }

    private void setJiraProjectKeys(List<String> keys) {
        listJiraProjectKeys.clear();
        listJiraProjectKeys.addAll(keys);
    }

}
