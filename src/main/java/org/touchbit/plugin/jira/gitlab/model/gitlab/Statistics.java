/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.gitlab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "commit_count",
    "storage_size",
    "repository_size",
    "lfs_objects_size",
    "job_artifacts_size"
})
public class Statistics {

    @JsonProperty("commit_count")
    private Integer commitCount;
    @JsonProperty("storage_size")
    private Integer storageSize;
    @JsonProperty("repository_size")
    private Integer repositorySize;
    @JsonProperty("lfs_objects_size")
    private Integer lfsObjectsSize;
    @JsonProperty("job_artifacts_size")
    private Integer jobArtifactsSize;

    @JsonProperty("commit_count")
    public Integer getCommitCount() {
        return commitCount;
    }

    @JsonProperty("commit_count")
    public void setCommitCount(Integer commitCount) {
        this.commitCount = commitCount;
    }

    @JsonProperty("storage_size")
    public Integer getStorageSize() {
        return storageSize;
    }

    @JsonProperty("storage_size")
    public void setStorageSize(Integer storageSize) {
        this.storageSize = storageSize;
    }

    @JsonProperty("repository_size")
    public Integer getRepositorySize() {
        return repositorySize;
    }

    @JsonProperty("repository_size")
    public void setRepositorySize(Integer repositorySize) {
        this.repositorySize = repositorySize;
    }

    @JsonProperty("lfs_objects_size")
    public Integer getLfsObjectsSize() {
        return lfsObjectsSize;
    }

    @JsonProperty("lfs_objects_size")
    public void setLfsObjectsSize(Integer lfsObjectsSize) {
        this.lfsObjectsSize = lfsObjectsSize;
    }

    @JsonProperty("job_artifacts_size")
    public Integer getJobArtifactsSize() {
        return jobArtifactsSize;
    }

    @JsonProperty("job_artifacts_size")
    public void setJobArtifactsSize(Integer jobArtifactsSize) {
        this.jobArtifactsSize = jobArtifactsSize;
    }

}
