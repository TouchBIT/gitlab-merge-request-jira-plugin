/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.gitlab;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "iid",
    "target_branch",
    "source_branch",
    "project_id",
    "title",
    "state",
    "created_at",
    "updated_at",
    "upvotes",
    "downvotes",
    "author",
    "assignee",
    "source_project_id",
    "target_project_id",
    "labels",
    "description",
    "work_in_progress",
    "milestone",
    "merge_when_pipeline_succeeds",
    "merge_status",
    "sha",
    "merge_commit_sha",
    "user_notes_count",
    "changes_count",
    "should_remove_source_branch",
    "force_remove_source_branch",
    "squash",
    "web_url",
    "discussion_locked",
    "time_stats",
    "approvals_before_merge"
})
public class MergeRequest {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("iid")
    private Integer iid;
    @JsonProperty("target_branch")
    private String targetBranch;
    @JsonProperty("source_branch")
    private String sourceBranch;
    @JsonProperty("project_id")
    private Integer projectId;
    @JsonProperty("title")
    private String title;
    @JsonProperty("state")
    private String state;
    @JsonProperty("created_at")
    private String createdAt;
    @JsonProperty("updated_at")
    private String updatedAt;
    @JsonProperty("upvotes")
    private Integer upvotes;
    @JsonProperty("downvotes")
    private Integer downvotes;
    @JsonProperty("author")
    private Author author;
    @JsonProperty("assignee")
    private Assignee assignee;
    @JsonProperty("source_project_id")
    private Integer sourceProjectId;
    @JsonProperty("target_project_id")
    private Integer targetProjectId;
    @JsonProperty("labels")
    private List<Object> labels = new ArrayList<>();
    @JsonProperty("description")
    private String description;
    @JsonProperty("work_in_progress")
    private Boolean workInProgress;
    @JsonProperty("milestone")
    private Milestone milestone;
    @JsonProperty("merge_when_pipeline_succeeds")
    private Boolean mergeWhenPipelineSucceeds;
    @JsonProperty("merge_status")
    private String mergeStatus;
    @JsonProperty("sha")
    private String sha;
    @JsonProperty("merge_commit_sha")
    private Object mergeCommitSha;
    @JsonProperty("user_notes_count")
    private Integer userNotesCount;
    @JsonProperty("changes_count")
    private String changesCount;
    @JsonProperty("should_remove_source_branch")
    private Boolean shouldRemoveSourceBranch;
    @JsonProperty("force_remove_source_branch")
    private Boolean forceRemoveSourceBranch;
    @JsonProperty("squash")
    private Boolean squash;
    @JsonProperty("web_url")
    private String webUrl;
    @JsonProperty("discussion_locked")
    private Boolean discussionLocked;
    @JsonProperty("time_stats")
    private TimeStats timeStats;
    @JsonProperty("approvals_before_merge")
    private Object approvalsBeforeMerge;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("iid")
    public Integer getIid() {
        return iid;
    }

    @JsonProperty("iid")
    public void setIid(Integer iid) {
        this.iid = iid;
    }

    @JsonProperty("target_branch")
    public String getTargetBranch() {
        return targetBranch;
    }

    @JsonProperty("target_branch")
    public void setTargetBranch(String targetBranch) {
        this.targetBranch = targetBranch;
    }

    @JsonProperty("source_branch")
    public String getSourceBranch() {
        return sourceBranch;
    }

    @JsonProperty("source_branch")
    public void setSourceBranch(String sourceBranch) {
        this.sourceBranch = sourceBranch;
    }

    @JsonProperty("project_id")
    public Integer getProjectId() {
        return projectId;
    }

    @JsonProperty("project_id")
    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("state")
    public String getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(String state) {
        this.state = state;
    }

    @JsonProperty("created_at")
    public String getCreatedAt() {
        return createdAt;
    }

    @JsonProperty("created_at")
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    @JsonProperty("updated_at")
    public String getUpdatedAt() {
        return updatedAt;
    }

    @JsonProperty("updated_at")
    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @JsonProperty("upvotes")
    public Integer getUpvotes() {
        return upvotes;
    }

    @JsonProperty("upvotes")
    public void setUpvotes(Integer upvotes) {
        this.upvotes = upvotes;
    }

    @JsonProperty("downvotes")
    public Integer getDownvotes() {
        return downvotes;
    }

    @JsonProperty("downvotes")
    public void setDownvotes(Integer downvotes) {
        this.downvotes = downvotes;
    }

    @JsonProperty("author")
    public Author getAuthor() {
        return author;
    }

    @JsonProperty("author")
    public void setAuthor(Author author) {
        this.author = author;
    }

    @JsonProperty("assignee")
    public Assignee getAssignee() {
        return assignee;
    }

    @JsonProperty("assignee")
    public void setAssignee(Assignee assignee) {
        this.assignee = assignee;
    }

    @JsonProperty("source_project_id")
    public Integer getSourceProjectId() {
        return sourceProjectId;
    }

    @JsonProperty("source_project_id")
    public void setSourceProjectId(Integer sourceProjectId) {
        this.sourceProjectId = sourceProjectId;
    }

    @JsonProperty("target_project_id")
    public Integer getTargetProjectId() {
        return targetProjectId;
    }

    @JsonProperty("target_project_id")
    public void setTargetProjectId(Integer targetProjectId) {
        this.targetProjectId = targetProjectId;
    }

    @JsonProperty("labels")
    public List<Object> getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(List<Object> labels) {
        this.labels = labels;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("work_in_progress")
    public Boolean getWorkInProgress() {
        return workInProgress;
    }

    @JsonProperty("work_in_progress")
    public void setWorkInProgress(Boolean workInProgress) {
        this.workInProgress = workInProgress;
    }

    @JsonProperty("milestone")
    public Milestone getMilestone() {
        return milestone;
    }

    @JsonProperty("milestone")
    public void setMilestone(Milestone milestone) {
        this.milestone = milestone;
    }

    @JsonProperty("merge_when_pipeline_succeeds")
    public Boolean getMergeWhenPipelineSucceeds() {
        return mergeWhenPipelineSucceeds;
    }

    @JsonProperty("merge_when_pipeline_succeeds")
    public void setMergeWhenPipelineSucceeds(Boolean mergeWhenPipelineSucceeds) {
        this.mergeWhenPipelineSucceeds = mergeWhenPipelineSucceeds;
    }

    @JsonProperty("merge_status")
    public String getMergeStatus() {
        return mergeStatus;
    }

    @JsonProperty("merge_status")
    public void setMergeStatus(String mergeStatus) {
        this.mergeStatus = mergeStatus;
    }

    @JsonProperty("sha")
    public String getSha() {
        return sha;
    }

    @JsonProperty("sha")
    public void setSha(String sha) {
        this.sha = sha;
    }

    @JsonProperty("merge_commit_sha")
    public Object getMergeCommitSha() {
        return mergeCommitSha;
    }

    @JsonProperty("merge_commit_sha")
    public void setMergeCommitSha(Object mergeCommitSha) {
        this.mergeCommitSha = mergeCommitSha;
    }

    @JsonProperty("user_notes_count")
    public Integer getUserNotesCount() {
        return userNotesCount;
    }

    @JsonProperty("user_notes_count")
    public void setUserNotesCount(Integer userNotesCount) {
        this.userNotesCount = userNotesCount;
    }

    @JsonProperty("changes_count")
    public String getChangesCount() {
        return changesCount;
    }

    @JsonProperty("changes_count")
    public void setChangesCount(String changesCount) {
        this.changesCount = changesCount;
    }

    @JsonProperty("should_remove_source_branch")
    public Boolean getShouldRemoveSourceBranch() {
        return shouldRemoveSourceBranch;
    }

    @JsonProperty("should_remove_source_branch")
    public void setShouldRemoveSourceBranch(Boolean shouldRemoveSourceBranch) {
        this.shouldRemoveSourceBranch = shouldRemoveSourceBranch;
    }

    @JsonProperty("force_remove_source_branch")
    public Boolean getForceRemoveSourceBranch() {
        return forceRemoveSourceBranch;
    }

    @JsonProperty("force_remove_source_branch")
    public void setForceRemoveSourceBranch(Boolean forceRemoveSourceBranch) {
        this.forceRemoveSourceBranch = forceRemoveSourceBranch;
    }

    @JsonProperty("squash")
    public Boolean getSquash() {
        return squash;
    }

    @JsonProperty("squash")
    public void setSquash(Boolean squash) {
        this.squash = squash;
    }

    @JsonProperty("web_url")
    public String getWebUrl() {
        return webUrl;
    }

    @JsonProperty("web_url")
    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @JsonProperty("discussion_locked")
    public Boolean getDiscussionLocked() {
        return discussionLocked;
    }

    @JsonProperty("discussion_locked")
    public void setDiscussionLocked(Boolean discussionLocked) {
        this.discussionLocked = discussionLocked;
    }

    @JsonProperty("time_stats")
    public TimeStats getTimeStats() {
        return timeStats;
    }

    @JsonProperty("time_stats")
    public void setTimeStats(TimeStats timeStats) {
        this.timeStats = timeStats;
    }

    @JsonProperty("approvals_before_merge")
    public Object getApprovalsBeforeMerge() {
        return approvalsBeforeMerge;
    }

    @JsonProperty("approvals_before_merge")
    public void setApprovalsBeforeMerge(Object approvalsBeforeMerge) {
        this.approvalsBeforeMerge = approvalsBeforeMerge;
    }

}
