/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.annotation.Nullable;
import java.util.List;

/**
 * Created by Oleg Shaburov on 15.04.2018
 * shaburov.o.a@gmail.com
 */
public class JSON {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static String toString(Object o) {
        try {
            return MAPPER.writeValueAsString(o);
        } catch (Exception e) {
            // formally there can be no mistake
            return String.valueOf(o);
        }
    }

    public static String toPrettyPrintString(Object o) {
        try {
            return MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(o);
        } catch (Exception e) {
            // formally there can be no mistake
            return String.valueOf(o);
        }
    }

    @Nullable
    public static <T> T map(String jsonString, Class<T> clazz) {
        try {
            return MAPPER.readValue(jsonString, clazz);
        } catch (Exception ignore) {
            return null;
        }
    }

    @Nullable
    public static <T> List<T> mapToList(String jsonString, Class<T> clazz) {
        try {
            return MAPPER.readValue(jsonString, MAPPER.getTypeFactory().constructCollectionType(List.class, clazz));
        } catch (Exception ignore) {
            return null;
        }
    }

    private JSON() { }

}
