/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.transport;

import feign.Feign;
import feign.Retryer;
import feign.jackson.JacksonDecoder;
import feign.jackson.JacksonEncoder;
import feign.slf4j.Slf4jLogger;
import okhttp3.OkHttpClient;
import org.touchbit.plugin.jira.gitlab.model.jlab.Connection;

import java.util.concurrent.TimeUnit;

import static feign.Logger.Level.FULL;
import static org.touchbit.plugin.jira.gitlab.transport.GetLabAPIVersion.V4;
import static org.touchbit.plugin.jira.gitlab.utils.SocketHelper.getTrustAllCrtSocketFactory;
import static org.touchbit.plugin.jira.gitlab.utils.SocketHelper.getTrustAllHostame;

/**
 * Created by Oleg Shaburov on 23.06.2018
 * shaburov.o.a@gmail.com
 */
public class GitLabBuilder {

    private static final TimeUnit TIME_UNIT = TimeUnit.SECONDS;
    private final String host;
    private boolean ignoreSSLErrors = false;
    private int connectTimeout = 10;
    private int readTimeout = 5;
    private PrivateTokenAuthRequestInterceptor authenticator;
    private OkHttpClient.Builder builder = new OkHttpClient.Builder();

    public GitLabBuilder(final Connection connection) {
        host = connection.getAddress().endsWith("/") ? connection.getAddress() : connection.getAddress() + "/";
        setAuthenticator(new PrivateTokenAuthRequestInterceptor(connection.getApiToken()));
    }

    public GitLab build() {
        if (ignoreSSLErrors) {
            builder.sslSocketFactory(getTrustAllCrtSocketFactory());
            builder.hostnameVerifier(getTrustAllHostame());
        }
        builder.connectTimeout(connectTimeout, TIME_UNIT);
        builder.readTimeout(readTimeout, TIME_UNIT);
        builder.addInterceptor(authenticator);
        return Feign.builder()
                .client(new feign.okhttp.OkHttpClient(builder.build()))
                .encoder(new JacksonEncoder())
                .decoder(new JacksonDecoder())
                .retryer(new Retryer.Default(0, 0, 0))
                .logger(new Slf4jLogger(GitLab.class))
                .logLevel(FULL)
                .target(GitLab.class, host + V4.get());
    }

    private void setAuthenticator(PrivateTokenAuthRequestInterceptor authenticator) {
        this.authenticator = authenticator;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public void setIgnoreSSLErrors(boolean ignoreSSLErrors) {
        this.ignoreSSLErrors = ignoreSSLErrors;
    }

}
