/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.runnable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.model.jlab.GlProject;
import org.touchbit.plugin.jira.gitlab.model.jlab.Relation;
import org.touchbit.plugin.jira.gitlab.transport.GitLab;

/**
 * Created by Oleg Shaburov on 16.04.2018
 * shaburov.o.a@gmail.com
 */
public class ProjectLoader {

    private static final Logger LOG = LoggerFactory.getLogger(ProjectLoader.class);

    public static void load(GitLab gitLab, Relation relation) {
        String jiraProjectKey = relation.getJiraProjectKey();
        for (GlProject glProject : relation.getGitlabProjects()) {
            Thread t = new Thread(new Loader(gitLab, jiraProjectKey, glProject));
            t.setName("JLab Project loader: " + jiraProjectKey);
            LOG.debug("Start ProjectLoader for jira project {}", jiraProjectKey);
            t.start();
        }
    }

    private static class Loader implements Runnable {

        private final GlProject glProject;
        private final String jiraProjectKey;
        private final GitLab api;

        private Loader(final GitLab gitLab, final String jiraProjectKey, final GlProject glProject) {
            this.jiraProjectKey = jiraProjectKey;
            this.glProject = glProject;
            this.api = gitLab;
        }

        @Override
        public void run() {
            try {
                Project gitlabProject;
                if (glProject.getProjectID() != null) {
                    gitlabProject = api.getProject(String.valueOf(glProject.getProjectID()));
                } else {
                    gitlabProject = api.getProject(String.valueOf(glProject.getProjectPathNameSpace()));
                }
                String gitLabProjectNameWithNamespace = gitlabProject.getPathWithNamespace();
                String searchLine = glProject.getSearchLine();
                gitlabProject.setSearchLine(searchLine);
                JLab.addGitlabProject(jiraProjectKey, gitlabProject);
                LOG.debug("Added JLab project: {} {} {}", jiraProjectKey, gitLabProjectNameWithNamespace, searchLine);
            } catch (Throwable e) {
                LOG.error("Error loading project: {}", glProject.getProjectID(), e);
            }
        }
    }

    /** Utility class. We prohibit instantiation. */
    private ProjectLoader() { }

}
