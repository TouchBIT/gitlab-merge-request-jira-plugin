/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.gitlab;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "issues",
    "merge_requests",
    "repo_branches",
    "labels",
    "events",
    "members"
})
public class Links {

    @JsonProperty("self")
    private String self;
    @JsonProperty("issues")
    private String issues;
    @JsonProperty("merge_requests")
    private String mergeRequests;
    @JsonProperty("repo_branches")
    private String repoBranches;
    @JsonProperty("labels")
    private String labels;
    @JsonProperty("events")
    private String events;
    @JsonProperty("members")
    private String members;

    @JsonProperty("self")
    public String getSelf() {
        return self;
    }

    @JsonProperty("self")
    public void setSelf(String self) {
        this.self = self;
    }

    @JsonProperty("issues")
    public String getIssues() {
        return issues;
    }

    @JsonProperty("issues")
    public void setIssues(String issues) {
        this.issues = issues;
    }

    @JsonProperty("merge_requests")
    public String getMergeRequests() {
        return mergeRequests;
    }

    @JsonProperty("merge_requests")
    public void setMergeRequests(String mergeRequests) {
        this.mergeRequests = mergeRequests;
    }

    @JsonProperty("repo_branches")
    public String getRepoBranches() {
        return repoBranches;
    }

    @JsonProperty("repo_branches")
    public void setRepoBranches(String repoBranches) {
        this.repoBranches = repoBranches;
    }

    @JsonProperty("labels")
    public String getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(String labels) {
        this.labels = labels;
    }

    @JsonProperty("events")
    public String getEvents() {
        return events;
    }

    @JsonProperty("events")
    public void setEvents(String events) {
        this.events = events;
    }

    @JsonProperty("members")
    public String getMembers() {
        return members;
    }

    @JsonProperty("members")
    public void setMembers(String members) {
        this.members = members;
    }

}
