/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

/**
 * Created by Oleg Shaburov on 21.04.2018
 * shaburov.o.a@gmail.com
 */
public class StringUtils {

    public static String base64Decode(String value) {
        if (value != null) {
            return new String(Base64.getDecoder().decode(value));
        }
        return "";
    }

    public static String base64Encode(String value) {
        if (value != null) {
            return new String(Base64.getEncoder().encode(value.getBytes()));
        }
        return "";
    }

    public static String encode(Object value) {
        String strValue = String.valueOf(value);
        try {
            return URLEncoder.encode(strValue, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException ignore) {
            return strValue;
        }
    }

    private StringUtils() {}

}
