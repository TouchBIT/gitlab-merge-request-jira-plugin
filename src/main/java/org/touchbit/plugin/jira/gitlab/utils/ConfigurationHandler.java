/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import com.atlassian.jira.security.groups.GroupManager;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.touchbit.plugin.jira.gitlab.model.jlab.Connection;
import org.touchbit.plugin.jira.gitlab.model.jlab.GlProject;
import org.touchbit.plugin.jira.gitlab.model.jlab.Relation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.touchbit.plugin.jira.gitlab.transport.GitLab;
import org.touchbit.plugin.jira.gitlab.transport.GitLabBuilder;

import java.util.List;
import java.util.StringJoiner;

import static org.touchbit.plugin.jira.gitlab.utils.StringUtils.base64Encode;

/**
 * Checker for {@link Configuration} object.
 * <p>
 * Created by Oleg Shaburov on 15.04.2018
 * shaburov.o.a@gmail.com
 */
public class ConfigurationHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationHandler.class);

    private static Configuration configuration;

    public static boolean check(final Configuration config, final GroupManager manager) {
        configuration = config;
        LOG.debug("Check configuration is initialized.");
        String group = configuration.getGroupOfPluginJiraAdmins();
        if (!isEmptyString(group) && !manager.groupExists(group)) {
            configuration.setPluginError("Specified group of plugin administrators does not exist");
        } else {
            LOG.debug("Group of plugin jira admins is correct");
        }
        checkConnections();
        if (isValidConfiguration()) {
            LOG.info("Received configuration is correct.");
            return true;
        }
        LOG.warn("Received configuration is incorrect.");
        return false;
    }

    private static void checkConnections() {
        List<Connection> connections = configuration.getConnections();
        if (connections.isEmpty()) {
            configuration.setPluginError("There should be at least one connection to gitlab");
        }
        for (Connection connection : connections) {
            if (isEmptyString(connection.getAddress())) {
                LOG.error("Field 'Address' is empty");
                connection.setCheckGitLabResult(base64Encode("The gitlab connection host should be specified"));
                setInvalidConfiguration();
            }
            if (isEmptyString(connection.getApiToken())) {
                LOG.error("Field 'Api token' is empty");
                connection.setCheckGitLabResult(base64Encode("The gitlab connection API token should be specified"));
                setInvalidConfiguration();
            }
            if (connection.getCheckGitLabResult() != null) {
                continue;
            }
            checkGitLabConnection(connection);
        }
    }

    @SuppressWarnings("squid:S1181")
    private static void checkGitLabConnection(Connection connection) {
        try {
            String host = connection.getAddress().trim();
            if (host.endsWith("/")) {
                host = host.substring(0, host.lastIndexOf('/'));
            }
            String token = connection.getApiToken().trim();
            connection.setAddress(host);
            connection.setApiToken(token);

            GitLab api = new GitLabBuilder(connection).build();
            LOG.debug("Successful connected to GitLab: {}", connection.getAddress());

            connection.setGitLabVersion(api.getVersion().getVersion());
            connection.setGitLabUser(api.getUser().getUsername());

            String result= "GitLab version: " + connection.getGitLabVersion() +
                    ". User: " + connection.getGitLabUser();

            connection.setCheckGitLabResult(base64Encode(result));
            checkRelations(api, connection);
        } catch (Throwable e) {
            connection.setCheckGitLabResult(base64Encode(e.getMessage()
                    .replaceFirst("[a-z]{1}", e.getMessage().substring(0, 1).toUpperCase())));
            setInvalidConfiguration();
            LOG.error("Field to connect to the GitLab {}", connection.getAddress(), e);
        }
    }

    private static void checkRelations(GitLab api, Connection connection) {
        List<Relation> relations = connection.getRelations();
        for (Relation relation : relations) {
            if (isEmptyString(relation.getJiraProjectKey())) {
                relation.setJiraError("Jira project key must be specified");
                setInvalidConfiguration();
            }
            if (relation.getGitlabProjects().isEmpty()) {
                relation.setJiraError("There should be specified at least one gitlab project");
                setInvalidConfiguration();
            } else {
                LOG.debug("Relation is correct");
                checkGitlabProjects(api, connection.getGitLabUser(), relation.getGitlabProjects());
            }
        }
    }

    private static void checkGitlabProjects(GitLab api, String userName, List<GlProject> glProjects) {
        for (GlProject glProject : glProjects) {
            String searchLine = glProject.getSearchLine().trim();
            if (isEmptyString(glProject.getProject())) {
                glProject.setProjectError("Gitlab project should be specified");
                setInvalidConfiguration();
            } else {
                checkProject(api, userName, glProject);
            }
            if (isEmptyString(searchLine)) {
                glProject.setSearchLineError("Search line should be specified");
                setInvalidConfiguration();
            } else {
                glProject.setSearchLine(searchLine);
                LOG.debug("Search line [{}] is correct", searchLine);
            }
        }
    }

    @SuppressWarnings("squid:S1181")
    private static void checkProject(GitLab api, String userName, GlProject glProject) {
        String project = glProject.getProject();
        if (isNumeric(project)) {
            Integer projectId = Integer.parseInt(project);
            try {
                Project gitlabProject = api.getProject(projectId.toString());
                glProject.setProjectID(gitlabProject.getId());
                glProject.setProject(gitlabProject.getPathWithNamespace());
                glProject.setProjectPathNameSpace(gitlabProject.getPathWithNamespace());
                LOG.debug("Project with ID={} is correct", projectId);
                return;
            } catch (Throwable e) {
                LOG.error("Field to get GitLab project by ID={}", projectId, e);
                if (e.getClass().getName().contains("java.net.")) {
                    glProject.setProjectError(e.getMessage());
                } else {
                    glProject.setProjectError("Gitlab project with identifier [" + projectId + "] not found, " +
                            "or user [" + userName + "] does not have access to the project");
                }
                setInvalidConfiguration();
                return;
            }
        }
        if (!project.contains("/") ||
                project.length() < 3 ||
                project.indexOf('/') == 0 ||
                project.lastIndexOf('/') == project.length() - 1
                ) {
            glProject.setProjectError("Expected [namespace/name] format or project ID (int)");
            setInvalidConfiguration();
            return;
        }
        try {
            StringJoiner sj = new StringJoiner("/");
            for (String s : project.split("/")) {
                sj.add(s.trim().replace(" ", "."));
            }
            Project gitlabProject = api.getProject(sj.toString());
            glProject.setProjectID(gitlabProject.getId());
            glProject.setProject(gitlabProject.getPathWithNamespace());
            glProject.setProjectPathNameSpace(gitlabProject.getPathWithNamespace());
            LOG.debug("Project [{}] is correct. Path with namespace: {}", project, gitlabProject.getPathWithNamespace());
        } catch (Throwable e) {
            LOG.error("Field to get GitLab project {}", project, e);
            glProject.setProjectError("Gitlab project [" + project + "] not found, " +
                    "or user [" + userName + "] does not have access to the project");
            setInvalidConfiguration();
        }
    }

    private static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        for (char c : str.trim().toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

    private static void setInvalidConfiguration() {
        configuration.setPluginError("Invalid configuration");
    }

    private static boolean isValidConfiguration() {
        return configuration.getPluginError() == null;
    }

    private static boolean isEmptyString(String s) {
        return s == null || s.trim().isEmpty();
    }

    /** Utility class. We prohibit instantiation. */
    private ConfigurationHandler() {}
}
