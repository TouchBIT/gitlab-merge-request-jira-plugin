/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.transport;

/**
 * Created by Oleg Shaburov on 23.06.2018
 * shaburov.o.a@gmail.com
 */
public enum  GetLabAPIVersion {

    V3 ("api/v3"),
    V4 ("api/v4"),
    ;

    private String version;

    GetLabAPIVersion(String version) {
        this.version = version;
    }

    public String get() {
        return version;
    }

}
