/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.transport;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Created by Oleg Shaburov on 24.06.2018
 * shaburov.o.a@gmail.com
 */
public class PrivateTokenAuthRequestInterceptor implements Interceptor {

    private String token;

    PrivateTokenAuthRequestInterceptor(String token) {
        this.token = token;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        Request request = original.newBuilder()
                .url(original.url())
                .headers(original.headers())
                .header("Private-Token", token)
                .method(original.method(), original.body())
                .build();
        return chain.proceed(request);
    }

}
