/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab;

import org.touchbit.plugin.jira.gitlab.model.gitlab.MergeRequest;
import org.touchbit.plugin.jira.gitlab.model.gitlab.Project;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.touchbit.plugin.jira.gitlab.model.jlab.Connection;
import org.openjdk.jol.info.GraphLayout;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.touchbit.plugin.jira.gitlab.runnable.ProjectLoader;
import org.touchbit.plugin.jira.gitlab.transport.GitLab;
import org.touchbit.plugin.jira.gitlab.transport.GitLabBuilder;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Plugin cache.
 * Contains gitlab connections, available projects and plugin projects with meta info.
 * <p>
 * Created by Oleg Shaburov on 14.04.2018
 * shaburov.o.a@gmail.com
 */
public class JLab {

    private static final Logger LOG = LoggerFactory.getLogger(JLab.class);

    /** Relation Jira project key -> GitLab endpoints */
    private static final Map<String, List<GitLab>> GIT_LAB_CONNECTIONS = new ConcurrentHashMap<>();
    /** Relation Jira project key -> GitLab projects */
    private static final Map<String, List<Project>> GIT_LAB_PROJECTS = new ConcurrentHashMap<>();

    private static Configuration configuration;

    private static boolean isInitialized = false;

    public static void init(final Configuration config) {
        clearCache();
        isInitialized = false;
        if (config != null) {
            setConfiguration(config);
            for (Connection connection : configuration.getConnections()) {
                GitLab gitLab = new GitLabBuilder(connection).build();
                connection.getRelations().forEach(r -> addGitLab(gitLab, r.getJiraProjectKey()));
                connection.getRelations().forEach(r -> ProjectLoader.load(gitLab, r));
            }
            isInitialized = true;
        } else {
            LOG.warn("The cache is not initialized. Cause: plugin not configured.");
        }
    }

    public static Map<Project, List<MergeRequest>> searchMRs(String jiraProjectKey, String jiraIssueKey) {
        Map<Project, List<MergeRequest>> map = new HashMap<>();
        try {
            GIT_LAB_PROJECTS.get(jiraProjectKey).forEach(p ->
                    GIT_LAB_CONNECTIONS.get(jiraProjectKey).forEach(g -> {
                        try {
                            List<MergeRequest> requests = g.searchProjectMergeRequests(p.getId().toString(),
                                    p.getSearchLine() + " " + jiraIssueKey);
                            map.put(p, requests);
                        } catch (Exception ignore) {
                            // TODO связь проект <-> gitlab
                        }
                    }));
        } catch (Exception e) {
            LOG.error("Project or GitLab connection not found. Check plugin configuration.");
        }
        return map;
    }

    public static void clearCache() {
        GIT_LAB_CONNECTIONS.clear();
        GIT_LAB_PROJECTS.clear();
        configuration = null;
    }

    public static synchronized void addGitLab(GitLab gitLab, String jiraProjectKey) {
        if (GIT_LAB_CONNECTIONS.get(jiraProjectKey) == null) {
            List<GitLab> gitLabs = new ArrayList<>();
            gitLabs.add(gitLab);
            GIT_LAB_CONNECTIONS.put(jiraProjectKey, gitLabs);
        } else {
            GIT_LAB_CONNECTIONS.get(jiraProjectKey).add(gitLab);
        }
        LOG.info("Added GitLab connection to the cache for Jira project key {}", jiraProjectKey);
    }

    public static void addGitlabProjects(final String jiraProjectKey, final List<Project> projects) {
        LOG.info("Add projects to the cache for {}", jiraProjectKey);
        projects.forEach(p -> addGitlabProject(jiraProjectKey, p));
    }

    public static synchronized void addGitlabProject(final String jiraProjectKey, final Project project) {
        LOG.info("Add projects to the cache for {}", jiraProjectKey);
        if (GIT_LAB_PROJECTS.get(jiraProjectKey) == null) {
            List<Project> projects = new ArrayList<>();
            projects.add(project);
            GIT_LAB_PROJECTS.put(jiraProjectKey, projects);
        } else {
            GIT_LAB_PROJECTS.get(jiraProjectKey).add(project);
        }
    }

    public static boolean jiraProjectKeyIsPresent(String jiraProjectKey) {
        return jiraProjectKey != null && GIT_LAB_PROJECTS.get(jiraProjectKey) != null;
    }

    public static String[] getCacheInfo() {
        if (configuration != null) {
            StringJoiner result = new StringJoiner("\n");
            result.add(getTotalSize());
            for (Connection connection : configuration.getConnections()) {
                result.add("GitLab host: " + connection.getAddress());
                result.add("GitLab version: " + connection.getGitLabVersion());
                result.add("GitLab user: " + connection.getGitLabUser());
                String errors = getErrors(connection);
                if (errors.length() > 0) {
                    result.add(errors);
                }
                String relations = getRelations(connection);
                if (relations.length() > 0) {
                    result.add(relations);
                }
                String availableProjects = getGitLabProjects(connection);
                if (availableProjects.length() > 0) {
                    result.add(availableProjects);
                }
            }
            return result.toString().split("\\n");
        }
        return new String[]{"The cache is not initialized. Cause: plugin not configured."};
    }

    private static String getTotalSize() {
        long connectionsSize = GraphLayout.parseInstance(GIT_LAB_CONNECTIONS).totalSize();
        long gitLabProjectsSize = GraphLayout.parseInstance(GIT_LAB_PROJECTS).totalSize();
        return "Total size: " + getConvertedSize(connectionsSize + gitLabProjectsSize);
    }

    private static String getGitLabProjects(Connection connection) {
        if (connection != null) {
            String host = connection.getAddress();
            StringJoiner projects = new StringJoiner("\n");
            GIT_LAB_PROJECTS.forEach((k, v) -> v.stream().filter(p -> k.equals(host))
                    .collect(Collectors.toList())
                    .forEach(r -> projects.add(r.getId() + ": " + r.getPathWithNamespace())));
            if (projects.length() > 0) {
                return "Available Projects:\n" + projects;
            }
        }
        return "";
    }

    private static String getRelations(Connection connection) {
        if (connection != null) {
            StringJoiner result = new StringJoiner("\n");
            connection.getRelations().forEach(r -> GIT_LAB_PROJECTS.get(r.getJiraProjectKey())
                    .forEach(p -> result.add(r.getJiraProjectKey() + ": " + p.getPathWithNamespace())));
            if (result.length() > 0) {
                return "Relations:\n" + result;
            }
        }
        return "";
    }

    private static String getErrors(Connection connection) {
        if (connection != null) {
            StringJoiner result = new StringJoiner("\n");
            connection.getRelations().forEach(r -> GIT_LAB_PROJECTS.get(r.getJiraProjectKey())
                    .stream().filter(p -> p.getError() != null).collect(Collectors.toList())
                    .forEach(p -> result.add(p.getError())));
            if (result.length() > 0) {
                return "Errors:\n" + result;
            }
        }
        return "";
    }

    /**
     * Getting the size of cache objects in readable form (byte, kilobyte, megabyte)
     */
    public static String getConvertedSize(long size) {
        String value;
        if (size < 0) {
            LOG.warn("Invalid size of cache objects - {}", size);
            return "0 B";
        }
        if (size < 1024) {
            value = size + " B";
        } else if (size < 1048576) {
            value = (size / 1024) + " KB";
        } else if (size < 1073741824) {
            value = (size / 1048576) + " MB";
        } else {
            value = (size / 1073741824) + " GB";
        }
        return value;
    }

    public static void setConfiguration(Configuration config) {
        configuration = config;
    }

    public static Configuration getConfiguration() {
        return configuration;
    }

    public static boolean isInitialized() {
        return isInitialized;
    }

    public static List<GitLab> getGitlabAPI(String jiraProjectKey) {
        return GIT_LAB_CONNECTIONS.get(jiraProjectKey);
    }

    public static List<Project> getGLProjects(String jiraProjectKey) {
        return GIT_LAB_PROJECTS.get(jiraProjectKey);
    }

    /** Utility class. We prohibit instantiation. */
    private JLab() {}

}
