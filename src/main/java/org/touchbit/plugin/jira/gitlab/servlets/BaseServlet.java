/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.servlets;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.security.groups.GroupManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.touchbit.plugin.jira.gitlab.utils.JSON;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.Map;

import static org.touchbit.plugin.jira.gitlab.utils.HttpServletRequestHandler.CONFIGURATION;

/**
 * Created by Oleg Shaburov on 15.04.2018
 * shaburov.o.a@gmail.com
 */
public abstract class BaseServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(BaseServlet.class);

    private static final String STORAGE = "org.touchbit.plugin.jira.gitlab.config";
    private static final ObjectMapper MAPPER = new ObjectMapper();

    private static final String TEMPLATE_CACHE = "templates/cache.vm";
    private static final String TEMPLATE_ADMIN = "templates/admin.vm";
    private static final String CONTENT_TYPE = "text/html; charset=utf-8";

    private final UserManager userManager;
    private final TemplateRenderer renderer;
    private final LoginUriProvider loginUriProvider;
    private final PluginSettings settings;
    private final GroupManager groupManager;

    public BaseServlet(final UserManager userManager,
                final LoginUriProvider loginUriProvider,
                final TemplateRenderer renderer,
                final PluginSettingsFactory factory) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.renderer = renderer;
        this.settings = factory.createSettingsForKey(STORAGE);
        this.groupManager = ComponentAccessor.getOSGiComponentInstanceOfType(GroupManager.class);
    }

    boolean userHasAccess(final HttpServletRequest request,
                          final HttpServletResponse response,
                          final Configuration configuration) {
        String username = getUserManager().getRemoteUsername(request);
        if (username == null) {
            LOG.error("Unauthorized user. Redirect to login page.");
            redirectToLogin(request, response);
            return false;
        }
        if (configuration != null) {
            String group = configuration.getGroupOfPluginJiraAdmins();
            if (!getUserManager().isSystemAdmin(username) &&
                    (group == null || !getUserManager().isUserInGroup(username, group))) {
                LOG.debug("User {} has no access to plugin configuration", username);
                try {
                    response.sendError(404);
                } catch (IOException e) {
                    LOG.error("Error occurred while try send 404 error code", e);
                }
                return false;
            }
            LOG.debug("User {} has access to plugin configuration", username);
        } else {
            LOG.warn("The plugin configuration was not found. Access is denied.");
            return false;
        }
        return true;
    }

    void renderCache(Map<String, Object> map, HttpServletResponse response) {
        render(TEMPLATE_CACHE, map, response);
    }

    void renderAdmin(Map<String, Object> map, HttpServletResponse response) {
        render(TEMPLATE_ADMIN, map, response);
    }

    private void render(String template, Map<String, Object> map, HttpServletResponse response) {
        response.setContentType(CONTENT_TYPE);
        try {
            renderer.render(template, map, response.getWriter());
        } catch (IOException e) {
            LOG.error("An error occurs when rendering response", e);
        }
    }

    private void redirectToLogin(final HttpServletRequest request, HttpServletResponse response) {
        try {
            response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
        } catch (IOException e) {
            LOG.error("Error occurred while try redirect to login page", e);
        }
    }

    private URI getUri(final HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    Configuration getSavedConfiguration() {
        LOG.debug("Get PluginSettings configuration body for key: {}", CONFIGURATION);
        Object body = settings.get(CONFIGURATION);
        LOG.debug("body: {}", body);
        try {
            return MAPPER.readValue(body.toString(), Configuration.class);
        } catch (Exception ignore) {
            LOG.debug("Created new configuration object");
            return new Configuration();
        }
    }

    void saveConfiguration(Configuration configuration) {
        LOG.debug("Try save configuration");
        try {
            String body = JSON.toString(configuration);
            LOG.debug("Save PluginSettings {}:{}",CONFIGURATION, body);
            settings.put(CONFIGURATION, body);
            LOG.debug("Init Configuration Caching");
            JLab.init(configuration);
        } catch (Exception e) {
            LOG.error("Error saving settings in Plugin settings storage", e);
            configuration.setPluginError("Error saving settings in Plugin settings storage: " + e.getMessage());
        }
    }

    protected UserManager getUserManager() {
        return userManager;
    }

    protected TemplateRenderer getTemplateRenderer() {
        return renderer;
    }

    protected LoginUriProvider getLoginUriProvider() {
        return loginUriProvider;
    }

    protected PluginSettings getPluginSettings() {
        return settings;
    }

    protected GroupManager getGroupManager() {
        return groupManager;
    }

}
