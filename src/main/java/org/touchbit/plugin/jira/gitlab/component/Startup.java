/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.component;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.touchbit.plugin.jira.gitlab.utils.JSON;

import static org.touchbit.plugin.jira.gitlab.utils.HttpServletRequestHandler.CONFIGURATION;

/**
 * The initialization class of the plugin configuration saved in the jira.
 * <p>
 * Created by Oleg Shaburov on 19.04.2018
 * shaburov.o.a@gmail.com
 */
public class Startup implements LifecycleAware {

    private static final Logger LOG = LoggerFactory.getLogger(Startup.class);

    private static final String STORAGE = "org.touchbit.plugin.jira.gitlab.config";

    private final PluginSettings settings;

    public Startup(@ComponentImport final PluginSettingsFactory factory) {
        this.settings = factory.createSettingsForKey(STORAGE);
        LOG.debug("Settings initialized");
    }

    @Override
    public void onStart() {
        LOG.info("Running the JLab plugin.");
        LOG.debug("Get PluginSettings configuration body for key: {}", CONFIGURATION);
        Object body = settings.get(CONFIGURATION);
        LOG.debug("body: {}", body);
        Configuration configuration = JSON.map(String.valueOf(body), Configuration.class);
        JLab.init(configuration);
    }

}
