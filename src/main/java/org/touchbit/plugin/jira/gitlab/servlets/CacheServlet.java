/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.servlets;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Servlet of the cache status page.
 *
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
public class CacheServlet extends BaseServlet {

    private static final Logger LOG = LoggerFactory.getLogger(CacheServlet.class);

    public CacheServlet(@ComponentImport final UserManager userManager,
                        @ComponentImport final LoginUriProvider loginUriProvider,
                        @ComponentImport final TemplateRenderer renderer,
                        @ComponentImport final PluginSettingsFactory factory) {
        super(userManager, loginUriProvider, renderer, factory);
    }

    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) {
        if (userHasAccess(request, response, JLab.getConfiguration())) {
            renderCache(configToMap(), response);
        } else {
            try {
                response.sendRedirect("/plugins/servlet/jlab/admin");
            } catch (IOException e) {
                LOG.error("Error occurred while try send error information", e);
            }
        }
    }

    private Map<String, Object> configToMap() {
        Map<String, Object> map = new HashMap<>();
        map.put("cacheInfo", JLab.getCacheInfo());
        return map;
    }

}
