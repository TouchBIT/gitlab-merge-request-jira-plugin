/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.jlab;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;

/**
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "project",
    "projectID",
    "projectPathName",
    "projectPathNameSpace",
    "projectError",
    "searchLine",
    "searchLineError"
})
public class GlProject {

    @JsonProperty("project")
    private String project;

    @JsonProperty("projectID")
    private Integer projectID;

    @JsonProperty("projectPathName")
    private String projectPathName;

    @JsonProperty("projectPathNameSpace")
    private String projectPathNameSpace;

    @JsonProperty("projectError")
    private String projectError;

    @JsonProperty("searchLine")
    private String searchLine;

    @JsonProperty("searchLineError")
    private String searchLineError;

    @JsonProperty("project")
    public String getProject() {
        return project;
    }

    @JsonProperty("project")
    public void setProject(String project) {
        this.project = project;
    }

    @JsonProperty("projectError")
    public String getProjectError() {
        return projectError;
    }

    @JsonProperty("projectError")
    public void setProjectError(String projectError) {
        this.projectError = projectError;
    }

    @JsonProperty("searchLine")
    public String getSearchLine() {
        return searchLine;
    }

    @JsonProperty("searchLine")
    public void setSearchLine(String searchLine) {
        this.searchLine = searchLine;
    }

    @JsonProperty("searchLineError")
    public String getSearchLineError() {
        return searchLineError;
    }

    @JsonProperty("searchLineError")
    public void setSearchLineError(String searchLineError) {
        this.searchLineError = searchLineError;
    }

    @JsonProperty("projectID")
    public Integer getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(Integer projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("projectPathName")
    public String getProjectPathName() {
        return projectPathName;
    }

    @JsonProperty("projectPathName")
    public void setProjectPathName(String projectPathName) {
        this.projectPathName = projectPathName;
    }

    @JsonProperty("projectPathNameSpace")
    public String getProjectPathNameSpace() {
        return projectPathNameSpace;
    }

    @JsonProperty("projectPathNameSpace")
    public void setProjectPathNameSpace(String projectPathNameSpace) {
        this.projectPathNameSpace = projectPathNameSpace;
    }

}
