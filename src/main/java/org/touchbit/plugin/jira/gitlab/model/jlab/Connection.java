/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.jlab;

import com.fasterxml.jackson.annotation.*;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "address",
    "apiToken",
    "checkGitLabResult",
    "relations"
})
public class Connection {

    @JsonProperty("address")
    private String address;

    @JsonProperty("apiToken")
    private String apiToken;

    @JsonProperty("gitLabUser")
    private String gitLabUser;

    @JsonProperty("gitLabVersion")
    private String gitLabVersion;

    @JsonProperty("checkGitLabResult")
    private String checkGitLabResult;

    @JsonProperty("relations")
    private List<Relation> relations = new ArrayList<>();

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("apiToken")
    public String getApiToken() {
        return apiToken;
    }

    @JsonProperty("apiToken")
    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    @JsonProperty("checkGitLabResult")
    public String getCheckGitLabResult() {
        return checkGitLabResult;
    }

    @JsonProperty("checkGitLabResult")
    public void setCheckGitLabResult(String checkGitLabResult) {
        this.checkGitLabResult = checkGitLabResult;
    }

    @JsonProperty("relations")
    public List<Relation> getRelations() {
        return relations;
    }

    @JsonProperty("relations")
    public void setRelations(List<Relation> relations) {
        this.relations = relations;
    }

    @JsonProperty("gitLabUser")
    public String getGitLabUser() {
        return gitLabUser;
    }

    @JsonProperty("gitLabUser")
    public void setGitLabUser(String gitLabUser) {
        this.gitLabUser = gitLabUser;
    }

    @JsonProperty("gitLabVersion")
    public String getGitLabVersion() {
        return gitLabVersion;
    }

    @JsonProperty("gitLabVersion")
    public void setGitLabVersion(String gitLabVersion) {
        this.gitLabVersion = gitLabVersion;
    }

}
