/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.model.jlab;

import com.fasterxml.jackson.annotation.*;
import org.touchbit.plugin.jira.gitlab.utils.JSON;

import javax.annotation.Generated;
import java.util.ArrayList;
import java.util.List;

/**
 * Gitlab configuration GET response
 * <p>
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@Generated("com.googlecode.jsonschema2pojo")
@JsonPropertyOrder({
    "jiraProjectKeys",
    "groupOfPluginJiraAdmins",
    "pluginError",
    "connections"
})
public class Configuration {

    @JsonProperty("jiraProjectKeys")
    private List<String> jiraProjectKeys = new ArrayList<>();

    @JsonProperty("groupOfPluginJiraAdmins")
    private String groupOfPluginJiraAdmins;

    @JsonProperty("pluginError")
    private PluginError pluginError;

    @JsonProperty("connections")
    private List<Connection> connections = new ArrayList<>();

    @JsonProperty("jiraProjectKeys")
    public List<String> getJiraProjectKeys() {
        return jiraProjectKeys;
    }

    @JsonProperty("jiraProjectKeys")
    public void setJiraProjectKeys(List<String> jiraProjectKeys) {
        this.jiraProjectKeys = jiraProjectKeys;
    }

    @JsonProperty("groupOfPluginJiraAdmins")
    public String getGroupOfPluginJiraAdmins() {
        return groupOfPluginJiraAdmins;
    }

    @JsonProperty("groupOfPluginJiraAdmins")
    public void setGroupOfPluginJiraAdmins(String groupOfPluginJiraAdmins) {
        this.groupOfPluginJiraAdmins = groupOfPluginJiraAdmins;
    }

    @JsonProperty("pluginError")
    public PluginError getPluginError() {
        return pluginError;
    }

    @JsonProperty("pluginError")
    public void setPluginError(PluginError pluginError) {
        this.pluginError = pluginError;
    }

    public void setPluginError(String msg) {
        pluginError = new PluginError();
        pluginError.setMsg(msg);
    }

    @JsonProperty("connections")
    public List<Connection> getConnections() {
        return connections;
    }

    @JsonProperty("connections")
    public void setConnections(List<Connection> connections) {
        this.connections = connections;
    }

    public List<String> getRelatedJiraProjectKeys() {
        final List<String> relatedJiraProjectKeys = new ArrayList<>();
        connections.forEach(c -> c.getRelations().forEach(r -> relatedJiraProjectKeys.add(r.getJiraProjectKey())));
        return relatedJiraProjectKeys;
    }

    @Override
    public String toString() {
        return JSON.toString(this);
    }

}
