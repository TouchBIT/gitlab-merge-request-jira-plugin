/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.web;

import com.atlassian.jira.issue.Issue;
import com.atlassian.plugin.web.Condition;
import org.touchbit.plugin.jira.gitlab.JLab;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

import static org.touchbit.plugin.jira.gitlab.web.GitLabLeftPanel.ISSUE;

/**
 * Web-panel display validation class (merge requests table).
 *
 * Created by Oleg Shaburov on 31.03.2018
 * shaburov.o.a@gmail.com
 */
public class ShowGitLabWebPanelCondition implements Condition {

    private static final Logger log = LoggerFactory.getLogger(ShowGitLabWebPanelCondition.class);

    @Override
    public void init(final Map<String, String> context) {
        //do nothing
    }

    @Override
    public boolean shouldDisplay(final Map<String, Object> context) {
        Issue issue;
        try {
            issue = (Issue) context.get(ISSUE);
        } catch (ClassCastException e) {
            log.error("{}: {}", e.getClass().getSimpleName(), e.getMessage());
            return false;
        }

        if (issue == null || issue.getProjectObject() == null) {
            log.warn("Issue or Issue project is null. GitLab merge request web panel will not be displayed.");
            return false;
        }
        String jiraIssueKey = issue.getKey();
        String jiraProjectKey = issue.getProjectObject().getKey();
        if (JLab.jiraProjectKeyIsPresent(jiraProjectKey)) {
            log.debug("Relations with GitLab projects for {} are found. " +
                    "The GitLabConnection web-panel will be displayed.", jiraIssueKey);
            return true;
        }
        log.debug("Not found Relationships with GitLab projects for {}. " +
                "The GitLabConnection web panel will not be displayed.", jiraIssueKey);
        return false;
    }

}
