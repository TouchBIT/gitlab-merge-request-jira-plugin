/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.touchbit.plugin.jira.gitlab.model.jlab.Configuration;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Created by Oleg Shaburov on 15.04.2018
 * shaburov.o.a@gmail.com
 */
public class HttpServletRequestHandler {

    private static final Logger LOG = LoggerFactory.getLogger(HttpServletRequestHandler.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private Map<String, String> itemsMap = new HashMap<>();

    private static final String ACTION = "action";
    private static final String SAVE   = "save";
    private static final String TEST   = "test";

    public static final String CONFIGURATION = "configuration";

    public HttpServletRequestHandler(HttpServletRequest request) {
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            addItems(upload.parseRequest(request));
        } catch (FileUploadException e) {
            LOG.error("Error parse request", e);
        }
    }

    private void addItems(List<FileItem> items) {
        items.forEach(i -> itemsMap.put(i.getFieldName(), i.getString()));
        if (LOG.isDebugEnabled()) {
            StringJoiner sj = new StringJoiner("\n");
            itemsMap.forEach((k, v) -> sj.add(k + " = " + v));
            LOG.debug("Request parameters:\n{}", sj);
        }
    }

    public Configuration getConfiguration() {
        try {
            return MAPPER.readValue(itemsMap.get(CONFIGURATION), Configuration.class);
        } catch (Exception ignore) {
            return null;
        }
    }

    public boolean isActionSave() {
        if (itemsMap.get(ACTION) != null && itemsMap.get(ACTION).equals(SAVE)) {
            LOG.debug("Save action received");
            return true;
        }
        return false;
    }

    public boolean isActionTest() {
        if (itemsMap.get(ACTION) != null && itemsMap.get(ACTION).equals(TEST)) {
            LOG.debug("Test action received");
            return true;
        }
        return false;
    }

}
