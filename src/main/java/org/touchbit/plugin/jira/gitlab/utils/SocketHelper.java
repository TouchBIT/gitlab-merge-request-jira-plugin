/*
 * Copyright 2018 Shaburov Oleg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.touchbit.plugin.jira.gitlab.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

/**
 * Created by Oleg Shaburov on 16.05.2018
 * shaburov.o.a@gmail.com
 */
public class SocketHelper {

    private static final Logger LOG = LoggerFactory.getLogger(SocketHelper.class);

    private static final TrustAllCertsManager TRUST_ALL_CERTS_MANAGER = new TrustAllCertsManager();
    private static final TrustAllHostname TRUST_ALL_HOSTNAME = new TrustAllHostname();

    public static SSLSocketFactory getTrustAllCrtSocketFactory() {
        SSLSocketFactory factory = null;
        try {
            final SSLContext sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, new X509TrustManager[]{TRUST_ALL_CERTS_MANAGER}, new SecureRandom());
            factory = sslContext.getSocketFactory();
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            LOG.error("Error creating SSLSocketFactory for trusting all certificates", e);
        }
        return factory;
    }

    public static class TrustAllHostname implements HostnameVerifier {

        @Override
        public boolean verify(String var1, SSLSession var2) {
            return true;
        }

    }

    public static TrustAllHostname getTrustAllHostame() {
        return TRUST_ALL_HOSTNAME;
    }

    public static X509TrustManager getTrustAllCrtManager() {
        return TRUST_ALL_CERTS_MANAGER;
    }

    private static class TrustAllCertsManager implements X509TrustManager {

        @Override
        public void checkClientTrusted(X509Certificate[] chain, String authType) {
            // do nothing
        }

        @Override
        public void checkServerTrusted(X509Certificate[] chain, String authType) {
            // do nothing
        }

        @Override
        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[0];
        }
    }

    /** Utility class. We prohibit instantiation. */
    private SocketHelper() {}
}
