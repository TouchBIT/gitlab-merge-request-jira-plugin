<?xml version="1.0" encoding="UTF-8"?>
<!--
  ~ Copyright 2018 Shaburov Oleg
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->
<atlassian-plugin key="${atlassian.plugin.key}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="TouchBIT" url="touchbit.org"/>
        <param name="plugin-icon">images/pluginIcon.png</param>
        <param name="plugin-logo">images/pluginLogo.png</param>
        <param name="configure.url">/plugins/servlet/jlab/admin</param>
        <param name="atlassian-data-center-compatible">true</param>
    </plugin-info>

    <component-import interface="com.atlassian.sal.api.pluginsettings.PluginSettingsFactory" key="pluginSettingsFactory" />
    <component-import interface="com.atlassian.sal.api.transaction.TransactionTemplate" key="transactionTemplate" />
    <component-import interface="com.atlassian.sal.api.user.UserManager" key="userManager" />
    <component-import interface="com.atlassian.sal.api.auth.LoginUriProvider" key="loginUriProvider" />
    <component-import interface="com.atlassian.templaterenderer.velocity.one.six.VelocityTemplateRenderer" key="velocity-renderer" />
    <component-import interface="com.atlassian.sal.api.ApplicationProperties" key="applicationProperties" />
    <component-import interface="com.atlassian.sal.api.message.I18nResolver" key="i18nResolver" />

    <component key="JLabStartup" class="org.touchbit.plugin.jira.gitlab.component.Startup" public="true"
               interface="com.atlassian.sal.api.lifecycle.LifecycleAware" />

    <web-resource name="Admin Web Resources" key="admin-resources">
        <dependency>com.atlassian.auiplugin:aui-experimental-expander</dependency>
        <resource type="download" name="index.js" location="js/index.js" />
        <resource type="download" name="formData.js" location="js/formData.js" />
        <resource type="download" name="components.js" location="js/components.js" />
        <resource type="download" name="relationTypeController.js" location="js/relationTypeController.js" />
        <resource type="download" name="admin.css" location="css/admin.css" />
    </web-resource>

    <web-section key="jlab-menu" name="GitLab" location="admin_plugins_menu" weight="110">
        <label>GitLab MR plugin</label>
        <description>Custom section for GitLab plugin administration</description>
    </web-section>

    <web-item key="jlab-menu-config-item" name="GitLab configuration" application="jira" section="admin_plugins_menu/jlab-menu" weight="10">
        <label>Configuration</label>
        <link linkId="jlab-admin-config-link">/plugins/servlet/jlab/admin</link>
        <description>Link to GitLab configuration page</description>
    </web-item>

    <web-item key="jlab-menu-cache-item" name="GitLab cache" application="jira" section="admin_plugins_menu/jlab-menu" weight="10">
        <label>Cache status</label>
        <link linkId="jlab-admin-cache-link">/plugins/servlet/jlab/cache</link>
        <description>GitLab plugin cache status page</description>
    </web-item>

    <servlet name="Admin servlet" class="org.touchbit.plugin.jira.gitlab.servlets.AdminServlet" key="jlab-admin-servlet">
        <resource name="i18n" location="i18n.admin" type="i18n" />
        <url-pattern>/jlab/admin</url-pattern>
        <description>GitLab plugin configuration page</description>
        <load-on-startup>1</load-on-startup>
    </servlet>

    <servlet name="Cache servlet" class="org.touchbit.plugin.jira.gitlab.servlets.CacheServlet" key="jlab-cache-servlet">
        <resource name="i18n" location="i18n.cache" type="i18n" />
        <url-pattern>/jlab/cache</url-pattern>
        <description>GitLab plugin cache status page</description>
    </servlet>

    <web-panel name="GitLab Panel"
               i18n-name-key="gitlab.panel.name"
               key="gitlabpanelmodule"
               location="atl.jira.view.issue.left.context"
               weight="750">
        <description key="gitlab.panel.description">Show GitLab merge requests in a View Issue panel</description>
        <label>GitLab merge requests</label>
        <resource name="i18n" location="i18n.web" type="i18n" />
        <resource name="view" location="templates/gitlab-left-panel.vm" type="velocity" />
        <context-provider class="org.touchbit.plugin.jira.gitlab.web.GitLabLeftPanel" />
        <condition        class="org.touchbit.plugin.jira.gitlab.web.ShowGitLabWebPanelCondition" />
        <supports-ajax-load>true</supports-ajax-load>
    </web-panel>

</atlassian-plugin>
