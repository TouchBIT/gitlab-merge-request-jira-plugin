/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
window.jlabPlugins = window.jlabPlugins || {};

window.jlabPlugins.formData = {
    get: function () {
        const form = document.querySelector('.jira-add-on-for-gitlab-content>form');
        const formConnectionList = form.querySelectorAll('.jira-add-on-for-gitlab-content__connection');

        return {
            groupOfPluginJiraAdmins: form.groupOfPluginJiraAdmins.value,
            connections: Array.from(formConnectionList).map(this.getDataFromConnection.bind(this))
        };
    },

    getDataFromConnection: function (connectionElement) {
        const address = connectionElement.querySelector('[name="address"]');
        const apiToken = connectionElement.querySelector('[name="apiToken"]');
        const relationList = connectionElement.querySelectorAll('.jira-add-on-for-gitlab-content__relations');

        return {
            address: address.value,
            apiToken: apiToken.value,
            relations: Array.from(relationList).map(this.getDataFromRelation.bind(this))
        };
    },

    getDataFromRelation: function (relationsElement) {
        const jiraProjectKey = relationsElement.querySelector('[name="jiraProjectKey"]');
        const gitlabProjectList = relationsElement.querySelectorAll(
            '.jira-add-on-for-gitlab-content__relations-reps-list-item'
        );

        return {
            jiraProjectKey: jiraProjectKey.value,
            gitlabProjects: Array.from(gitlabProjectList).map(this.getDataFromGitlabProjects.bind(this))
        };
    },

    getDataFromGitlabProjects: function (gitlabProjectElement) {
        const project = gitlabProjectElement.querySelector('[name="project"]');
        const searchLine = gitlabProjectElement.querySelector('[name="searchLine"]');

        return {
            project: project.value,
            searchLine: searchLine.value
        }
    }
};
