/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
window.jlabPlugins = window.jlabPlugins || {};

window.jlabPlugins.RelationTypeController = function (typeList, relationContainerElement, addRelationButton) {
    const disableOptionsFunction = function () {
        const projectKeysElements = relationContainerElement.querySelectorAll('[name="jiraProjectKey"]');
        const usedKeys = [];

        projectKeysElements.forEach(function (projectKey) {
            const projectKeyValue = projectKey.value;

            if (projectKeyValue) {
                usedKeys.push(projectKeyValue);
            }

            Array.from(projectKey.children).forEach(function (item) {
                item.disabled = false;
            });
        });

        usedKeys.forEach(function (usedKey) {
            relationContainerElement.querySelectorAll('[name="jiraProjectKey"]:not([value="' + usedKey + '"]')
                .forEach(function (item) {
                    item.querySelector('[value="' + usedKey + '"]').disabled = true;
                });
        });
    };

    const observer = new MutationObserver(function (mutationRecordList) {
        addRelationButton.disabled = relationContainerElement.children.length === typeList.length;

        mutationRecordList.forEach(function (mutationRecord) {
            mutationRecord.addedNodes.forEach(function (relationElement) {
                disableOptionsFunction();

                relationElement.querySelector('[name="jiraProjectKey"]')
                    .addEventListener('change', disableOptionsFunction);
            });

            mutationRecord.removedNodes.forEach(function () {
                disableOptionsFunction();
            });
        });
    });

    observer.observe(relationContainerElement, {
        childList: true
    });

    return {
        disconnect: function () {
            observer.disconnect();
        }
    }
};