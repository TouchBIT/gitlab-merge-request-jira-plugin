/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
window.jlabPlugins = window.jlabPlugins || {};

window.jlabPlugins.components = {

    createSelect: function (list) {
        const selectTemplate = document.createElement('select');
        const emptyOptionElement = document.createElement('option');
        selectTemplate.className = 'select';
        selectTemplate.name = 'jiraProjectKey';
        selectTemplate.appendChild(emptyOptionElement);

        list.forEach(function (item) {
            const optionElement = document.createElement('option');
            optionElement.value = item;
            optionElement.textContent = item;

            selectTemplate.appendChild(optionElement);
        });
//        selectTemplate.style.cssText = 'max-width: 110px;padding: 6px 5px 5px 5px;vertical-align: top;'
        return function (value) {
            const selectNode = selectTemplate.cloneNode(true);
            selectNode.value = value || '';

            return selectNode;
        }
    },

    createLabel: function (text, isRequired) {
        const labelElement = document.createElement('label');
        labelElement.textContent = text;

        if (isRequired) {
            const requiredElement = document.createElement('span');
            requiredElement.className = 'aui-icon icon-required';
            requiredElement.textContent = '(required)';

            labelElement.appendChild(requiredElement);
        }

        return labelElement;
    },

    createTextInput: function () {
        const inputTemplate = document.createElement('input');

        inputTemplate.type = 'text';
        inputTemplate.className = 'text';

        return function (name, value, placeholder) {
            const inputElement = inputTemplate.cloneNode(false);

            inputElement.placeholder = placeholder || '';
            inputElement.value = value || '';
            inputElement.name = name;

            return inputElement;
        };
    }(),

    createPasswordInput: function () {
            const inputTemplate = document.createElement('input');

            inputTemplate.type = 'password';
            inputTemplate.className = 'password';

            return function (name, value, placeholder) {
                const inputElement = inputTemplate.cloneNode(false);

                inputElement.placeholder = placeholder || '';
                inputElement.value = value || '';
                inputElement.name = name;

                return inputElement;
            };
    }(),

    createButton: function (options) {
        const buttonElement = document.createElement('button');

        buttonElement.className = 'aui-button';
        buttonElement.textContent = options.text || '';

        if (options.name) {
            buttonElement.name = options.name;
        }

        if (options.value) {
            buttonElement.value = options.value;
        }

        if (options.type) {
            buttonElement.type = options.type;
        }

        if (options.style) {
            buttonElement.style = options.style;
        }

        return buttonElement
    },

    createDetailError: function (data) {
        const errorContainer = document.createElement('div');
        errorContainer.className = 'jira-add-on-for-gitlab-error';

        const errorHeader = document.createElement('div');
        errorHeader.className = 'jira-add-on-for-gitlab-error-header';
        errorHeader.textContent = data.msg;
        errorContainer.appendChild(errorHeader);

        const errorContent = document.createElement('div');
        errorContent.className = 'aui-expander-content';
        errorContent.id = 'jira-add-on-for-gitlab-error-content';
        errorContainer.appendChild(errorContent);

        return errorContainer;
    },

    createErrorText: function (text) {
        const containerElement = document.createElement('div');
        containerElement.className = 'error';
        containerElement.textContent = text || '';

        return containerElement;
    },

    createRepositoryItemField: function (name, value, error, placeholder) {
        const containerElement = document.createElement('div');
        containerElement.className = 'jira-add-on-for-gitlab-content__relations-reps-list-item-field';

        const field = this.createTextInput(name, value, placeholder);
        containerElement.appendChild(field);
        containerElement.appendChild(this.createErrorText(error));

        return containerElement;
    },

    createRepositoryItem: function (data) {
        data = data || {};
        const containerElement = document.createElement('div');
        containerElement.className = 'jira-add-on-for-gitlab-content__relations-reps-list-item';

        containerElement.appendChild(
            this.createRepositoryItemField(
                'project',
                data.project,
                data.projectError,
                'GitLab project name'
            )
        );
        containerElement.appendChild(
            this.createRepositoryItemField(
                'searchLine',
                data.searchLine,
                data.searchLineError,
                'Search line'
            )
        );

        const buttonDelete = this.createButton({text: 'Delete', style: 'background-color:#ffd6d6'});
        buttonDelete.addEventListener('click', function (event) {
            event.target.parentNode.remove();
        });
        containerElement.appendChild(buttonDelete);

        return containerElement;
    },

    createTextFieldGroup: function (options) {
        const fieldId = options.name + Math.floor(Math.random() * 10000);
        const containerElement = document.createElement('div');
        containerElement.className = 'field-group';

        const label = this.createLabel(options.label, options.isRequired);
        label.htmlFor = fieldId;

        if (options.isSecure) {
            input = this.createPasswordInput(options.name, options.value, options.placeholder);
        } else {
            input = this.createTextInput(options.name, options.value, options.placeholder);
        }
        input.id = fieldId;

        const msgContainer = document.createElement('div');
        msgContainer.textContent = options.msg;

        containerElement.appendChild(label);
        containerElement.appendChild(input);
        containerElement.appendChild(msgContainer);
        return containerElement;
    },

    createCheckBox: function (name, value) {
        const checkBoxId = name + Math.floor(Math.random() * 10000);
        const containerElement = document.createElement('div');
        containerElement.className = 'checkbox';

        const field = document.createElement('input');
        field.id = checkBoxId;
        field.className = 'checkbox';
        field.type = 'checkbox';
        field.name = name || '';
        try {
            field.checked = value !== 'false' ? Boolean(value) : false;
        } catch (ignore) {
            field.checked = false;
        }

        const label = this.createLabel('Error information');
        label.htmlFor = checkBoxId;

        containerElement.appendChild(field);
        containerElement.appendChild(label);

        return containerElement;
    },

    createGroupsOfPluginsSection: function (value) {
        const containerElement = document.createDocumentFragment();

        containerElement.appendChild(this.createLabel('Group of plugin admins (Jira): '));
        containerElement.appendChild(this.createTextInput('groupOfPluginJiraAdmins', value, 'group_of_plugin'));

        return containerElement;
    },

    createSendForm: function () {
        const buttonActionGroupContainer = document.createElement('form');
        buttonActionGroupContainer.method = 'post';
        buttonActionGroupContainer.enctype = 'multipart/form-data';
        const configuration = this.createTextInput('configuration');
        configuration.classList.add('jira-add-on-for-gitlab__configuration-field');

        buttonActionGroupContainer.addEventListener('submit', function () {
            configuration.value = JSON.stringify(window.jlabPlugins.formData.get())
        });

        buttonActionGroupContainer.appendChild(configuration);
        buttonActionGroupContainer.appendChild(
            this.createButton({
                text: 'Test',
                name: 'action',
                value: 'test',
                type: 'submit'
            })
        );
        buttonActionGroupContainer.appendChild(
            this.createButton({
                text: 'Save',
                name: 'action',
                value: 'save',
                type: 'submit'
            })
        );

        return buttonActionGroupContainer;
    },

    createPluginButtonsSection: function (formElement, jiraProjectKeys) {
        const buttonGroupContainer = document.createElement('div');
        buttonGroupContainer.className = 'jira-add-on-for-gitlab-content__button-section';

        const buttonElement = this.createButton({text: 'Add connection'});
        buttonElement.addEventListener('click', function () {
            formElement.appendChild(
                this.createConnectionSection(jiraProjectKeys)
            );
        }.bind(this));
        buttonGroupContainer.appendChild(buttonElement);
        buttonGroupContainer.appendChild(this.createSendForm());

        return buttonGroupContainer;
    },

    createPlugin: function (data) {
        this.createSelect = this.createSelect(data.jiraProjectKeys);

        const connections = data.connections || [];
        const contentElement = document.querySelector('.jira-add-on-for-gitlab-content');
        const loaderElement = document.querySelector('.jira-add-on-for-gitlab-loader');

        const formElement = document.createElement('form');
        formElement.className = 'aui';

        const groupsOfPluginsSection = this.createGroupsOfPluginsSection(data.groupOfPluginJiraAdmins);
        formElement.appendChild(groupsOfPluginsSection);

        connections.forEach(function (value) {
            formElement.appendChild(this.createConnectionSection(data.jiraProjectKeys, value));
        }.bind(this));

        const buttonGroupContainer = this.createPluginButtonsSection(formElement, data.jiraProjectKeys);
        formElement.appendChild(buttonGroupContainer);

        loaderElement.remove();
        contentElement.appendChild(formElement);
        contentElement.appendChild(buttonGroupContainer);

        if (data.pluginError) {
            const errorContainer = document.createElement('div');
            errorContainer.className = 'jira-add-on-for-gitlab-content__common-error-section';
            errorContainer.appendChild(this.createDetailError(data.pluginError));
            contentElement.appendChild(errorContainer);
        }
    },

    createProjectKeySection: function (jiraProjectKey, jiraError) {
        const projectKeySection = document.createElement('div');
        projectKeySection.className = 'jira-add-on-for-gitlab-content__relations-project-key';
        projectKeySection.appendChild(this.createSelect(jiraProjectKey));
        projectKeySection.appendChild(this.createErrorText(jiraError));

        return projectKeySection;
    },

    createRepositoryButtonSection: function (repositorySectionsList, relationListItem) {
        const buttonGroupContainerElement = document.createElement('div');
        buttonGroupContainerElement.className = 'jira-add-on-for-gitlab-content__button-section';

        const addRepositoryButton = this.createButton({text: 'Add repository'});
        addRepositoryButton.addEventListener('click', function () {
            repositorySectionsList.appendChild(
                this.createRepositoryItem()
            );
            event.preventDefault();
        }.bind(this));
        buttonGroupContainerElement.appendChild(addRepositoryButton);

        const deleteRelationButton = this.createButton({text: 'Delete relation', style: 'background-color:#ffd6d6'});
        deleteRelationButton.addEventListener('click', function () {
            const relationList = relationListItem.parentNode;
            const connectionElement = relationList.parentNode;

            relationListItem.remove();

            if (!relationList.children.length) {
                const header = connectionElement.querySelector('.jira-add-on-for-gitlab-content__relation-header');
                if (header) {
                    header.remove()
                }
            }

            event.preventDefault();
        }.bind(this));
        buttonGroupContainerElement.appendChild(deleteRelationButton);

        return buttonGroupContainerElement;
    },

    createRepositorySection: function (gitlabProjects, relationListItem) {
        const repositorySections = document.createElement('div');
        repositorySections.className = 'jira-add-on-for-gitlab-content__relations-reps';

        const repositorySectionsList = document.createElement('div');
        repositorySectionsList.className = 'jira-add-on-for-gitlab-content__relations-reps-list';

        if (gitlabProjects && gitlabProjects.length) {
            gitlabProjects.forEach(function (gitlabProject) {
                repositorySectionsList.appendChild(
                    this.createRepositoryItem(gitlabProject)
                );
            }.bind(this));
        } else {
            repositorySectionsList.appendChild(
                this.createRepositoryItem()
            );
        }

        repositorySections.appendChild(repositorySectionsList);
        repositorySections.appendChild(
            this.createRepositoryButtonSection(repositorySectionsList, relationListItem)
        );

        return repositorySections;
    },

    createRelationSection: function (data) {
        data = data || '';
        const containerElement = document.createElement('div');
        containerElement.className = 'jira-add-on-for-gitlab-content__relations-list-item';

        const relationSection = document.createElement('div');
        relationSection.classList.add(
            'jira-add-on-for-gitlab-content__separate-section',
            'jira-add-on-for-gitlab-content__relations'
        );

        relationSection.appendChild(
            this.createProjectKeySection(data.jiraProjectKey, data.jiraError)
        );
        relationSection.appendChild(
            this.createRepositorySection(data.gitlabProjects, containerElement)
        );

        containerElement.appendChild(relationSection);
        containerElement.appendChild(this.createErrorText(data.jiraError));

        return containerElement;
    },

    createHeaderConnectionSection: function () {
        const containerTemplate = document.createElement('div');
        containerTemplate.className = 'jira-add-on-for-gitlab-content__separate-section';
        containerTemplate.classList.add(
            'jira-add-on-for-gitlab-content__separate-section',
            'jira-add-on-for-gitlab-content__connection'
        );

        const headerElement = document.createElement('h3');
        headerElement.textContent = 'Connection:';
        containerTemplate.appendChild(headerElement);

        return function (data) {
            const containerElement = containerTemplate.cloneNode(true);
            var checkGitLabResult;
            try {
                checkGitLabResult = atob(data.checkGitLabResult);
            } catch (err) {
                checkGitLabResult = data.checkGitLabResult;
            }
            containerElement.appendChild(
                this.createTextFieldGroup({
                    name: 'address',
                    label: 'Address: ',
                    value: data.address,
                    placeholder: 'address',
                    isRequired: true,
                    isSecure: false
                })
            );
            containerElement.appendChild(
                this.createTextFieldGroup({
                    name: 'apiToken',
                    label: 'Api token: ',
                    value: data.apiToken,
                    msg: checkGitLabResult,
                    placeholder: 'token',
                    isRequired: true,
                    isSecure: true
                })
            );

            return containerElement;
        }
    }(),

    createButtonsConnectionSection: function (projectList, relationList) {
        const containerElement = document.createElement('div');
        containerElement.className = 'jira-add-on-for-gitlab-content__button-section';

        const addProjectButton = this.createButton({text: 'Add relation'});
        addProjectButton.addEventListener('click', function (event) {
            if (!relationList.children.length) {
                relationList.parentNode.insertBefore(
                    this.createRelationHeader(),
                    relationList
                );
            }

            relationList.appendChild(this.createRelationSection());

            event.preventDefault();
        }.bind(this));
        containerElement.appendChild(addProjectButton);

        const relationTypeController = window.jlabPlugins.RelationTypeController(
            projectList,
            relationList,
            addProjectButton
        );

        const deleteConnectionButton = this.createButton({text: 'Delete connection', style: 'background-color:#ffd6d6'});
        deleteConnectionButton.addEventListener('click', function (event) {
            const connectionContainer = event.target.parentNode.parentNode;

            relationTypeController.disconnect();
            connectionContainer.remove();
            event.preventDefault();
        }.bind(this));
        containerElement.appendChild(deleteConnectionButton);

        return containerElement;
    },

    createRelationHeader: function () {
        const headerTemplate = document.createElement('h3');
        headerTemplate.className = 'jira-add-on-for-gitlab-content__relation-header';
        headerTemplate.textContent = 'Relations:';

        return function () {
            return headerTemplate.cloneNode(true);
        }
    }(),

    createConnectionSection: function (projectList, data) {
        data = data || {};

        const relations = data.relations || [];
        const containerElement = this.createHeaderConnectionSection(data);

        if (relations.length) {
            containerElement.appendChild(this.createRelationHeader());
        }

        const relationList = document.createElement('div');
        relationList.className = 'jira-add-on-for-gitlab-content__relation-list';
        const buttonGroup = this.createButtonsConnectionSection(projectList, relationList);

        relations.forEach(function (relation) {
            relationList.appendChild(this.createRelationSection(relation));
        }.bind(this));

        containerElement.appendChild(relationList);
        containerElement.appendChild(buttonGroup);

        return containerElement;
    }
};
