# JIRA plugin for integration with GitLab
Allows to show the merge requests associated with the issues in Jira

## Settings

##### Allows you to configure:
* multiple connections to GitLab   
* the plug-in administration group   
* dependencies between Jira projects and Gitlab repositories
* text commands in merge description allowing to connect that merge request with a Jira ticket

![Settings](src/main/resources/images/screenshot_002.png)

## Web panel

![Settings](src/main/resources/images/screenshot_001.png)